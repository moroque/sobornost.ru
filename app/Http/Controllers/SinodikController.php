<?php

namespace App\Http\Controllers;

use App\Models\Sinodik;
use Illuminate\Http\Request;

use Image;
use Carbon\Carbon;
use App\Exports\SinodikExport;
use Maatwebsite\Excel\Facades\Excel;

class SinodikController extends Controller
{

	protected $order_raw = 'патриарх, митрополит, архиепископ, схиепископ, епископ, схиархимандрит, архимандрит, протопресвитер, протоиерей, схиигумен, игумен, иеромонах, иерей, протодиакон, иеродиакон, диакон, схимонах, монах, схиигумения, инокиня-схимница, игумения, схимонахиня, монахиня, иподиакон, чтец, алтарник, регент, певчий, певчая, послушник, послушница, белая сестра, матушка';
	protected $order_raw_fixed = 'патриарх, митрополит, архиепископ, схиепископ, епископ, схиархимандрит, архимандрит, протопресвитер, протоиерей, схиигумен, игумен, иеромонах, иерей, протодиакон, иеродиакон, диакон, схимонах, монах, схиигумения, инокиня-схимница, игумения, схимонахиня, монахиня, иподиакон, чтец, алтарник';

	public function front(Request $request)
	{
		$total = Sinodik::count();
		$last_update = Sinodik::orderBy('updated_at', 'desc')->wherePublished(true)->first();

		if($last_update){
			$date = new Carbon($last_update->updated_at);
			$last_update = $date->format('d.m.Y');
		} else {
			$last_update = 'никогда';
		}

		$data = Sinodik::orderBy('country')->orderBy('deceased', 'desc')->wherePublished(true)->get();

		$data->map(function($row) {
			if($row['age'] > 10 && $row['age'] < 15){
				$row['age_str'] = ' лет';
			} else {
				//switch($row['age']-floor($row['age']/10)*10){
				switch(mb_substr($row['age'], -1)){
					case 1:
						$row['age_str'] = $row['age'].' год';
						break;
					case 2:
					case 3:
					case 4:
						$row['age_str'] = $row['age'].' года';
						break;
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 0:
						$row['age_str'] = $row['age'].' лет';
						break;
				}
			}
			
			return $row;
		});

		$by_country = $data->groupBy('country');
		if(isset($by_country['Россия'])){
			$total_in_russia = count($by_country['Россия']);
		} else {
			$total_in_russia = 0;
		}

		return view('sinodik.new', [ 
			'sinodik' => [ 
				'total' => $total, 
				'total_in_russia' => $total_in_russia, 
				'last_update' => $last_update, 
				'data' => $by_country->all() 
			] 
		]);
	}

	public function front_new(Request $request)
	{
		$total = Sinodik::count();
		$last_update = Sinodik::orderBy('updated_at', 'desc')->wherePublished(true)->first();

		if($last_update){
			$date = new Carbon($last_update->updated_at);
			$last_update = $date->format('d.m.Y');
		} else {
			$last_update = 'никогда';
		}

		$data = Sinodik::orderBy('country')->orderBy('deceased', 'desc')->wherePublished(true)->get();

		$data->map(function($row) {
			if($row['age'] > 10 && $row['age'] < 15){
				$row['age_str'] = ' лет';
			} else {
				//switch($row['age']-floor($row['age']/10)*10){
				switch(mb_substr($row['age'], -1)){
					case 1:
						$row['age_str'] = $row['age'].' год';
						break;
					case 2:
					case 3:
					case 4:
						$row['age_str'] = $row['age'].' года';
						break;
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 0:
						$row['age_str'] = $row['age'].' лет';
						break;
				}
			}
			
			return $row;
		});

		$by_country = $data->groupBy('country');
		if(isset($by_country['Россия'])){
			$total_in_russia = count($by_country['Россия']);
		} else {
			$total_in_russia = 0;
		}

		return view('sinodik.new', [ 
			'sinodik' => [ 
				'total' => $total, 
				'total_in_russia' => $total_in_russia, 
				'last_update' => $last_update, 
				'data' => $by_country->all() 
			] 
		]);
	}

	public function russia(Request $request)
	{
		$order = explode(', ', $this->order_raw_fixed);

		$total = Sinodik::whereCountry('Россия')->count();
		$last_update = Sinodik::orderBy('updated_at', 'desc')->wherePublished(true)->whereCountry('Россия')->first();

		if($last_update){
			$date = new Carbon($last_update->updated_at);
			$last_update = $date->format('d.m.Y');
		} else {
			$last_update = 'никогда';
		}

		$data = Sinodik::orderBy('country')->orderBy('deceased', 'asc')->wherePublished(true)->whereCountry('Россия')->get();

		$data = $data->filter(function($row) use ($order){
			return in_array(mb_strtolower($row['san']), $order);
		});

		if($request->covid == 'covid'){
			$data = $data->filter(function($row){
				return mb_strpos(mb_strtolower($row['reason']), 'ковид') !== false;
			});
		}

		$data->map(function($row){
/*			if(!is_integer($row['age'])){
				return $row;
			}*/

			if($row['age'] > 10 && $row['age'] < 15){
				$row['age_str'] = ' лет';
			} else {
				//switch($row['age']-floor($row['age']/10)*10){
				switch(mb_substr($row['age'], -1)){
					case 1:
						$row['age_str'] = $row['age'].' год';
						break;
					case 2:
					case 3:
					case 4:
						$row['age_str'] = $row['age'].' года';
						break;
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 0:
						$row['age_str'] = $row['age'].' лет';
						break;
				}
			}
			
			return $row;
		});

		$by_country = $data->groupBy('country');
		if(isset($by_country['Россия'])){
			$total_in_russia = count($by_country['Россия']);
		} else {
			$total_in_russia = 0;
		}

		return view('sinodik.russia', [ 
			'sinodik' => [ 
				'total' => $total, 
				'total_in_russia' => $total_in_russia, 
				'last_update' => $last_update, 
				'data' => $by_country->all(),
				'covid' => $request->covid
			] 
		]);
	}

	public function list(Request $request)
	{
		$last_update = Sinodik::orderBy('updated_at', 'desc')->wherePublished(true)->first();

		$countries = Sinodik::orderBy('country', 'asc')->groupBy('country')->get()->pluck('country')->filter()->toArray();

		return view('sinodik.list', [ 
			'sinodik' => [ 
				'total' => Sinodik::count(), 
				'last_update' => $last_update->updated_at->format('d.m.Y'), 
			],
			'countries' => array_values($countries)
		]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		if(!auth()->user()->hasPermissionTo('view sinodiks')){
			return response()->json([
				'error' => 'Access restricted'
			]);
		}

		$sinodiks = Sinodik::orderBy($request->sortField, $request->sortOrder);

		if(count($request->filters)){
			foreach($request->filters as $filter=>$query){
				$sinodiks = $sinodiks->whereRaw('LOWER('.$filter.') LIKE "%'.$query.'%"');
			}
		}

		if(count($request->global_filters)){
			if(in_array('only_own', $request->global_filters)){
				$sinodiks = $sinodiks->where('created_by', auth()->user()->id);
			}
			if(in_array('last_day', $request->global_filters)){
				$sinodiks = $sinodiks->where('created_at', '>', Carbon::now()->subHours(24));
			}
			if(in_array('only_unpublished', $request->global_filters)){
				$sinodiks = $sinodiks->where('published', false);
			}
			if(in_array('only_commented', $request->global_filters)){
				$sinodiks = $sinodiks->whereNotNull('inner_comment');
			}

		}

		$sinodiks = $sinodiks->paginate($request->per_page);

		$sinodiks->getCollection()->transform(function ($value) {
			return $this->rps($value);
		});

		return compact('sinodiks');
	}

	private function rps($value)
	{
		if(strlen($value->san) > 0){
			if(mb_substr($value->san, -1) == 'я' || mb_substr($value->san, -1) == 'а'){
				$gender = \morphos\Gender::FEMALE;
			} else {
				$gender = \morphos\Gender::MALE;
			}
		} else {
			if(mb_substr($value->lastname, -1) == 'а'){
				$gender = \morphos\Gender::FEMALE;
			} else {
				$gender = \morphos\Gender::MALE;
			}
		}
		$value->gender = $gender;
		if(!$value->san_rp){
			$value->san_rp = \morphos\Russian\inflectName($value->san, 'родительный', $gender);
		}

		if(mb_strlen($value->lastname) > 0 && mb_strlen($value->name) > 0){
			$tmp = explode(" ", \morphos\Russian\inflectName($value->lastname.' '.$value->name, 'родительный', $gender));
			if(isset($tmp[1])){
				if(!$value->name_rp){
					$value->name_rp = mb_strtoupper($tmp[1]);
				}
			}
			if(isset($tmp[0])){
				if(!$value->lastname_rp){
					$value->lastname_rp = $tmp[0];
				}
			}
		} else {
			if(mb_strlen($value->lastname) == 0){
				if(!$value->name_rp){
					$value->name_rp = \morphos\Russian\inflectName($value->name, 'родительный', $gender);
				}
			}
		}

		$value->name = mb_strtoupper($value->name);
		$value->name_rp = mb_strtoupper($value->name_rp);

		return $value;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$input = $request->all();

		$sinodik = Sinodik::create($input);

		$sinodik = $this->rps($sinodik);

		return compact('sinodik');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Sinodik  $sinodik
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Sinodik $sinodik)
	{
		$input = $request->all();

		$sinodik->update($input);

		$sinodik = $this->rps($sinodik);

		return compact('sinodik');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Sinodik  $sinodik
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Sinodik $sinodik)
	{
		$this->destroy_photo($sinodik);
		$sinodik->delete();

		return [
			'success' => 'ok'
		];
	}

	public function upload_photo(Sinodik $sinodik, Request $request)
	{
		$file = $request->file('photo');
		$fileNameToStore = $sinodik->id.'.jpg';

		$this->resizeImage($file, $fileNameToStore);

		return [
			'main' => \Storage::disk('public')->url('sinodik_photos/'.$sinodik->id.'.jpg'),
			'preview' => \Storage::disk('public')->url('sinodik_photos/preview_'.$sinodik->id.'.jpg')
		];
	}

	public function destroy_photo(Sinodik $sinodik)
	{
		\Storage::disk('public')->delete('sinodik_photos/'.$sinodik->id.'.jpg');
		\Storage::disk('public')->delete('sinodik_photos/preview_'.$sinodik->id.'.jpg');
	}

	public function resizeImage($file, $fileNameToStore) {
		// Resize image
		$resize = Image::make($file)->resize(400, null, function ($constraint) {
			$constraint->aspectRatio();
		})->encode('jpg');

		// Create preview
		$resize_preview = Image::make($file)->fit(150, 150, function ($constraint) {
			$constraint->upsize();
		})->encode('jpg');

		// Put image to storage
		$path = \Storage::disk('public')->put("sinodik_photos/{$fileNameToStore}", $resize->__toString());
		// Put preview to storage
		\Storage::disk('public')->put("sinodik_photos/preview_{$fileNameToStore}", $resize_preview->__toString());

		if($path) {
			return $path;
		}

		return false;
	}

	public function export() 
	{
		return Excel::download(new SinodikExport, 'sinodik_'.date('Y-m-d H:i:s').'.xlsx');
	}

	public function public_list_data(Request $request)
	{
		$order = explode(', ', $this->order_raw);
		

		$total = Sinodik::count();
		$last_update = Sinodik::orderBy('deceased', 'desc')->wherePublished(true)->first();
		$date = new Carbon($last_update->updated_at);

		$data = Sinodik::wherePublished(true);

		switch($request->covid){
			default:
			case 'all':
				break;
			case 'covid':
				$data = $data->where('reason', 'LIKE', '%ковид%');
				break;
			case 'noncovid':
				$data = $data->where('reason', 'NOT LIKE', '%ковид%');
				break;
		}

		if($request->countries){
			$countries = $request->countries;
			$data = $data->where(function($query) use ($countries){
				foreach($countries as $country){
					$query->orWhere('country', $country);
				}
			});
		}

		$date_2020 = Carbon::createFromDate(2020, 2, 23);
		$date_2021 = Carbon::createFromDate(2021, 2, 23);

		switch($request->range){
			case 'seven_days':
				$data = $data->where('deceased', '>', Carbon::now()->subDays(7));
				break;
			case 'thirty_days':
				$data = $data->where('deceased', '>', Carbon::now()->subDays(30));
				break;
			default:
			case 'fourty_days':
				$data = $data->where('deceased', '>', Carbon::now()->subDays(40));
				break;
			case 'three_months':
				$data = $data->where('deceased', '>', Carbon::now()->subMonths(3));
				break;
			case 'six_months':
				$data = $data->where('deceased', '>', Carbon::now()->subMonths(6));
				break;
			case 'one_year':
				$data = $data->where('deceased', '>', Carbon::now()->subYears(1));
				break;
			case '2020_year':
				$data = $data->where('deceased', '>', $date_2020->copy()->startOfYear())->where('deceased', '<=', $date_2020->copy()->endOfYear());
				break;
			case '2021_year':
				$data = $data->where('deceased', '>', $date_2021->copy()->startOfYear())->where('deceased', '<=', $date_2021->copy()->endOfYear());
				break;
			case 'all':
				break;
		}

		$data = $data->get(['id', 'name', 'lastname', 'san', 'country']);

		$total_selected = $data->count();

		$data->transform(function ($value) {
			return $this->rps($value);
		});

		$list = [];

		foreach($data as $line){
			$list[mb_strtolower($line->san)][] = $line;
		}

		$result = [];

		foreach($order as $i=>$san){
			if(isset($list[$san])){
				$result[$i] = $list[$san];
				unset($list[$san]);
			}
		}

		$chunked_struct = [];
		$chunked_unstruct = [];

		foreach($result as $s=>$san){
			$chunked_struct[$s] = array_chunk($san, ceil(count($san)/2));
		}

		foreach($list as $s=>$san){
			$chunked_unstruct[$s] = array_chunk($san, ceil(count($san)/2));
		}

		return [
			'total' => $total_selected, 
			'last_update' => $date->format('d.m.Y H:m'), 
			'data_struct' => $chunked_struct,
			'data_unstruct' => $chunked_unstruct,
		];
	}

	public function public_data(Request $request)
	{
		$data = Sinodik::wherePublished(true)->orderBy('country')->orderBy('deceased', 'desc');

		switch($request->covid){
			default:
			case 'all':
				break;
			case 'covid':
				$data = $data->where('reason', 'LIKE', '%ковид%');
				break;
			case 'noncovid':
				$data = $data->where('reason', 'NOT LIKE', '%ковид%');
				break;
		}

		if($request->countries){
			$countries = $request->countries;
			$data = $data->where(function($query) use ($countries){
				foreach($countries as $country){
					$query->orWhere('country', $country);
				}
			});
		}

		$date_2020 = Carbon::createFromDate(2020, 2, 23);
		$date_2021 = Carbon::createFromDate(2021, 2, 23);

		switch($request->range){
			case 'seven_days':
				$data = $data->where('deceased', '>', Carbon::now()->subDays(7));
				break;
			case 'thirty_days':
				$data = $data->where('deceased', '>', Carbon::now()->subDays(30));
				break;
			default:
			case 'fourty_days':
				$data = $data->where('deceased', '>', Carbon::now()->subDays(40));
				break;
			case 'three_months':
				$data = $data->where('deceased', '>', Carbon::now()->subMonths(3));
				break;
			case 'six_months':
				$data = $data->where('deceased', '>', Carbon::now()->subMonths(6));
				break;
			case 'one_year':
				$data = $data->where('deceased', '>', Carbon::now()->subYears(1));
				break;
			case '2020_year':
				$data = $data->where('deceased', '>', $date_2020->copy()->startOfYear())->where('deceased', '<=', $date_2020->copy()->endOfYear());
				break;
			case '2021_year':
				$data = $data->where('deceased', '>', $date_2021->copy()->startOfYear())->where('deceased', '<=', $date_2021->copy()->endOfYear());
				break;
			case 'all':
				break;
		}

		$data = $data->get();

		$data->map(function($row) {
			if($row['age'] > 10 && $row['age'] < 15){
				$row['age_str'] = ' лет';
			} else {
				//switch($row['age']-floor($row['age']/10)*10){
				switch(mb_substr($row['age'], -1)){
					case 1:
						$row['age_str'] = $row['age'].' год';
						break;
					case 2:
					case 3:
					case 4:
						$row['age_str'] = $row['age'].' года';
						break;
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 0:
						$row['age_str'] = $row['age'].' лет';
						break;
				}
			}
			
			return $row;
		});

		$by_country = $data->groupBy('country');

		return [
			'by_country' => $by_country,
			'total' => $data->count()
		];
	} 
}