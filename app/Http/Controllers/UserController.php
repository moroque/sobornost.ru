<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users = User::all();
		$permissions_list = Permission::all()->pluck('name');

		$permissions = [];
		foreach($permissions_list as $permission){
			$permission = explode(' ', $permission);
			$permissions[$permission[1]][$permission[0]] = true;
		}

		return compact('users', 'permissions');
	}

	public function show()
	{
		$user = auth()->user();
		$user->load('permissions:id,name');
		$user->load('roles:id,name');

		return compact('user');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = User::create([
			'name' => $request->user['name'],
			'email' => $request->user['email'],
			'password' => Hash::make($request->user['password'])
		]);

		return compact('user');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, User $user)
	{
		$data = [
			'name' => $request->user['name'],
			'email' => $request->user['email']
		];

		if(isset($request->user['password'])){
			$data['password'] = Hash::make($request->user['password']);
		}

		$user->update($data);

		if(count($request->permissions) > 0){
			$user->syncPermissions($request->permissions);
		}

		return compact('user');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user)
	{
		$user->delete();

		return [
			'success' => 'ok'
		];
	}
}
