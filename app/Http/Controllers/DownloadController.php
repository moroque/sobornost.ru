<?php

namespace App\Http\Controllers;

use App\Models\Download;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DownloadController extends Controller
{
    public function index(Request $request)
    {
    	$downloads = Download::orderBy($request->sortField, $request->sortOrder)->paginate($request->per_page);

    	return compact('downloads');
    }

    public function stats()
    {
		$by_country = DB::table('downloads')
						->select('country', DB::raw('count(*) as total'))
						->groupBy('country')
						->get();

		return $by_country;
    }

    public function deduplicate()
    {
		$duplicates = DB::table('downloads')
						//->select('subject', 'book_id')
						->select('email', 'ip')
						->selectRaw('COUNT(*) as count')
						//->groupBy('subject', 'book_id')
						->groupBy('email', 'ip')
						->havingRaw('COUNT(*) > 1')
						->get();

		foreach($duplicates as $dup){
			$dupy = Download::whereEmail($dup->email)->whereIp($dup->ip)->get(['id', 'email', 'ip']);
			$dupy->splice(1, 5);
			$ids = $dupy->pluck('id');
			Download::whereId($ids)->delete();
			//dd($ids);
			//$dupy->delete();
		}

		return $duplicates;
    }
}
