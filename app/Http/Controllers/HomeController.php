<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sinodik;
use App\Models\Testimonial;
use App\Models\Download;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function panel_stats()
    {
    	// sinodik
    	$sinodik_total = Sinodik::count();
    	$sinodik_countries = Sinodik::wherePublished(true)->select(DB::raw('country, count(*) as total'))->groupBy('country')->orderBy('total', 'desc')->get();
    	$sinodik_covid = Sinodik::wherePublished(true)->where('reason', 'LIKE', '%ковид%')->count();
    	$sinodik_unpublished = Sinodik::wherePublished(false)->count();
    	$sinodik_commented = Sinodik::whereNotNull('inner_comment')->count();

    	// downloads
    	$downloads_total = Download::count();
    	$downloads_countries = Download::select(DB::raw('country, count(*) as total'))->groupBy('country')->orderBy('total', 'desc')->get();

    	// testimonails
    	$testimonials_total = Testimonial::count();
    	$testimonials_unpublished = Testimonial::wherePublished(false)->count();
    	//$downloads_countries = Download::select(DB::raw('country, count(*) as total'))->groupBy('country')->orderBy('total', 'desc')->get();
    	return [
    		'sinodik' => [
    			'total' => $sinodik_total,
    			'countries' => $sinodik_countries,
    			'covid_count' => $sinodik_covid,
    			'unpublished' => $sinodik_unpublished,
    			'commented' => $sinodik_commented
    		],
    		'downloads' => [
    			'total' => $downloads_total,
    			'countries' => $downloads_countries
    		],
    		'testimonials' => [
    			'total' => $testimonials_total,
    			'unpublished' => $testimonials_unpublished,
    		]
    	];
    }
}
