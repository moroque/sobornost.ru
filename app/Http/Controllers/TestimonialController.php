<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;

use Image;
use Carbon\Carbon;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$testimonials = Testimonial::orderBy('id', 'desc');

/*		if(count($request->filters)){
			foreach($request->filters as $filter=>$query){
				$testimonials = $testimonials->whereRaw('LOWER('.$filter.') LIKE "%'.$query.'%"');
			}
		}*/

		if(count($request->global_filters)){
			if(in_array('only_own', $request->global_filters)){
				$testimonials = $testimonials->where('created_by', auth()->user()->id);
			}
			if(in_array('last_day', $request->global_filters)){
				$testimonials = $testimonials->where('created_at', '>', Carbon::now()->subHours(24));
			}
			if(in_array('only_unpublished', $request->global_filters)){
				$testimonials = $testimonials->where('published', false);
			}
		}

		//$testimonials = $testimonials->paginate($request->per_page);
		$testimonials = $testimonials->get();

		return compact('testimonials');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$input = $request->all();

		$testimonial = Testimonial::create($input);

		return compact('testimonial');
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Sinodik  $sinodik
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Testimonial $testimonial)
	{
		$input = $request->all();

		$testimonial->update($input);

		return compact('testimonial');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Sinodik  $sinodik
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Testimonial $testimonial)
	{
		$this->destroy_photo($testimonial);
		$testimonial->delete();

		return [
			'success' => 'ok'
		];
	}

	public function upload_photo(Testimonial $testimonial, Request $request)
	{
		$file = $request->file('photo');
		$fileNameToStore = $testimonial->id.'.jpg';

		$this->resizeImage($file, $fileNameToStore);

		return [
			'main' => \Storage::disk('public')->url('testimonials_photos/'.$testimonial->id.'.jpg'),
			'preview' => \Storage::disk('public')->url('testimonials_photos/preview_'.$testimonial->id.'.jpg')
		];
	}

	public function destroy_photo(Testimonial $testimonial)
	{
		\Storage::disk('public')->delete('testimonials_photos/'.$testimonial->id.'.jpg');
		\Storage::disk('public')->delete('testimonials_photos/preview_'.$testimonial->id.'.jpg');
	}

	public function resizeImage($file, $fileNameToStore) {
		// Resize image
		$resize = Image::make($file)->resize(400, null, function ($constraint) {
			$constraint->aspectRatio();
		})->encode('jpg');

		// Create preview
		$resize_preview = Image::make($file)->fit(150, 150, function ($constraint) {
			$constraint->upsize();
		})->encode('jpg');

		// Put image to storage
		$path = \Storage::disk('public')->put("testimonials_photos/{$fileNameToStore}", $resize->__toString());
		// Put preview to storage
		\Storage::disk('public')->put("testimonials_photos/preview_{$fileNameToStore}", $resize_preview->__toString());

		if($path) {
			return $path;
		}

		return false;
	}
}
