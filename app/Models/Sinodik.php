<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Sqits\UserStamps\Concerns\HasUserStamps;

class Sinodik extends Model
{
	use HasFactory;
	use HasUserStamps;

	protected $fillable = [
		'san', 'san_rp', 'name', 'name_rp', 'lastname', 'lastname_rp',
		'age', 'deceased', 'reason', 'country', 'e', 'town', 'place', 'role', 'lock', 'comment', 'published', 'inner_comment'
	];

	protected $appends = ['photo', 'photo_preview'];
	protected $casts = [
		'deceased' => 'date:d.m.Y'
	];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'deceased'
    ];

	public function getDeceasedAttribute($value)
	{
		return empty($value) ? null : (new Carbon($value))->tz(env('timezone'))->format('d.m.Y');
	}

	public function getPhotoAttribute()
	{
		if(\Storage::disk('public')->exists('sinodik_photos/'.$this->id.'.jpg')){
			return \Storage::url('sinodik_photos/'.$this->id.'.jpg');
		}

		return false;
	}

	public function getPhotoPreviewAttribute()
	{
		if(\Storage::disk('public')->exists('sinodik_photos/preview_'.$this->id.'.jpg')){
			return \Storage::url('sinodik_photos/preview_'.$this->id.'.jpg');
		}

		return false;
	}
}
