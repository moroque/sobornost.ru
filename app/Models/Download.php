<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;

    protected $fillable = ['email', 'geo', 'name', 'lastname', 'country', 'region', 'city', 'ip', 'created_at', 'updated_at'];

    protected $casts = [
    	'geo' => 'array'
    ];
}
