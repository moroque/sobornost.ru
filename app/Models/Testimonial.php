<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use HasFactory;

    protected $fillable = ['text', 'title', 'person', 'person_title', 'excerpt', 'published'];

    protected $appends = ['photo', 'photo_preview'];

	public function getPhotoAttribute()
	{
		if(\Storage::disk('public')->exists('testimonials_photos/'.$this->id.'.jpg')){
			return \Storage::url('testimonials_photos/'.$this->id.'.jpg');
		}

		return false;
	}

	public function getPhotoPreviewAttribute()
	{
		if(\Storage::disk('public')->exists('testimonials_photos/preview_'.$this->id.'.jpg')){
			return \Storage::url('testimonials_photos/preview_'.$this->id.'.jpg');
		}

		return false;
	}
}
