<?php

namespace App\Exports;

use App\Models\Sinodik;
use Maatwebsite\Excel\Concerns\FromCollection;

class SinodikExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Sinodik::all();
    }
}
