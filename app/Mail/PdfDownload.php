<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PdfDownload extends Mailable
{
    use Queueable, SerializesModels;

    public $mail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	return $this->subject('Семинар Соборность. Сборник № 1')
    				->attach(storage_path('app/pdf/pdf.pdf'), [
    					'as' => 'Семинар Соборность - Системные проблемы Православия (Выпуск №1).pdf',
    					'mime' => 'application/pdf',
    				])
    				->attach(storage_path('app/pdf/pdf2021_1.pdf'), [
    					'as' => 'Семинар Соборность - Системные проблемы Православия (Выпуск №2).pdf',
    					'mime' => 'application/pdf',
    				])
    				->markdown('emails.pdfdownload');
    }
}
