import Vue from 'vue'
import Router from 'vue-router'

import NotFoundPage from '@/components/errors/404'
import Panel from '@/components/Panel'

const DownloadsLog = () => import(/* webpackChunkName: "downloads_log" */ '@/components/DownloadsLog')
const Sinodik = () => import(/* webpackChunkName: "sinodik" */ '@/components/Sinodik')
const About = () => import(/* webpackChunkName: "about" */ '@/components/About')
const Docs = () => import(/* webpackChunkName: "docs" */ '@/components/Docs')
const Testimonials = () => import(/* webpackChunkName: "testimonials" */ '@/components/Testimonials')
const Users = () => import(/* webpackChunkName: "users" */ '@/components/Users')

Vue.use(Router)

const routes = [
	{
		path: '/s',
		name: 'panel',
		component: Panel,
		props: true,
	},
	{
		path: '/s/downloads',
		name: 'downloads_log',
		component: DownloadsLog,
		props: true,
	},
	{
		path: '/s/sinodik',
		name: 'sinodik',
		component: Sinodik,
		props: true,
	},
	{
		path: '/s/testimonials',
		name: 'testimonials',
		component: Testimonials,
		props: true,
	},
	{
		path: '/s/about',
		name: 'about',
		component: About,
		props: true,
	},
	{
		path: '/s/docs',
		name: 'docs',
		component: Docs,
		props: true,
	},
	{
		path: '/s/users',
		name: 'users',
		component: Users,
		props: true,
	},
	// Unknown
	{ path: '*', component: NotFoundPage }
]

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: routes,
	linkActiveClass: "active",
	linkExactActiveClass: "exact-active",
})

router.beforeEach((to, from, next) => {
	axios.get('/app/user').then(r => {
		router.app.$root.user = r.data.user

		next()
		// next({ name: 'userboard'})
	})
})

export default router