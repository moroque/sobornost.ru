import router from './router'
import App from '@/components/layout/App.vue'

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)

import titleMixin from '@/mixins/titleMixin'
Vue.mixin(titleMixin)

import moment from 'moment';
moment.locale('ru');
Vue.prototype.$moment = moment

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router,
    el: '#app',
    render: h => h(App),
    data(){
        return {
            user: null,
            sections: {
                downloads: 'Загрузки',
                testimonials: 'Отзывы',
                sinodiks: 'Синодик',
                users: 'Пользователи'
            }
        }
    },
    methods: {
        user_can(action, subject){
            if(
                this.user.permissions_tree[subject] == undefined ||
                this.user.permissions_tree[subject][action] == undefined ||
                this.user.permissions_tree[subject][action] == false
            ){
                return false
            }

            return true
        }
    }
});
