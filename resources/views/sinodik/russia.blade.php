<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Проект «Соборность»: Список умерших священников Русской Церкви</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Умершие от ковида и других причин епископы, священники и монахи Русской Православной Церкви">
    <meta name="author" content="Проект Соборность">    

    <meta property="og:title" content="Список умерших священников Русской Церкви" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://sobornost.ru/sinodik/russia" />
    <meta property="og:image" content="https://sobornost.ru/assets/images/covers/sinodik_fb_cover.jpg" />
    <meta property="og:site_name" content="Список умерших священников Русской Церкви" />

    <meta name="google-site-verification" content="ikoQ_xrqo3RLlUl4ZUvRf6VuQFtOr5pOQozuleBzLEQ" />
    
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700|Roboto:400,400i,700&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="/assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="/assets/css/theme.css">
    <link id="theme-style" rel="stylesheet" href="/assets/css/custom.css">

<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-516152-3kRh6"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-516152-3kRh6" style="position:fixed; left:-999px;" alt=""/></noscript>

</head> 

<body>    
    <header class="header">        
        <div class="branding">
            <div class="container-fluid position-relative">
                <div class="logo-wrapper">
                    <div class="site-logo">
                        <a class="navbar-brand m-0 p-0" href="/">
                            <img class="logo-icon" src="/assets/images/site-logo.svg" alt="logo" style="height:210px; ">
                        </a>
                    </div>    
                </div><!--//docs-logo-wrapper-->
            
            </div><!--//container-->
        </div><!--//branding-->
    </header><!--//header-->

    <section id="description-section" class="benefits-section theme-bg-light-gradient py-5">
        <div class="container py-5">
            <h2 class="section-heading text-center mb-3">СИНОДИК (Россия)<br> почившие от коронавирусной инфекции и других причин епископы, священнослужители и монашествующие</h2>
            <h4 class="text-center">
                Последнее обновление {{ $sinodik['last_update'] }}
            </h4>
            
            <div class="section-intro single-col-max mx-auto text-center mb-5">
                К сожалению, полноценной официальной статистики о зараженных и умерших церковно- и священнослужителях нет, 
                редакторы-добровольцы проекта "Соборность" с мая 2020 года и до сегодняшнего дня ведут и уточняют синодик. 
                Спасибо всем, кто принял участие в работе над ним. 
            </div>
            <div class="section-intro single-col-max mx-auto text-center mb-5">
                Если вы можете уточнить или дополнить наш синодик, пишите на <a href="mailto:sobornost.ru@gmail.com">sobornost.ru@gmail.com</a> 
                или на нашу страницу в фб <a href="https://www.facebook.com/sobornost.project" target="_blank">https://www.facebook.com/sobornost.project</a><br>
                Друзья, сделайте репост, пригласите друзей помянуть умерших в своих молитвах!
            </div>

            <div class="text-center">
                @if($sinodik['covid'])
                    <a class="btn btn-lg btn-primary" href="/sinodik/russia">Показать полный список</a>
                @else
                    <a class="btn btn-lg btn-primary" href="/sinodik/russia/covid">Показать только ковид</a>
                @endif
            </div>

            @foreach ($sinodik['data'] as $country=>$in_country)
                <h2 class="section-heading text-center my-4" id="{{ $country }}">
                    {{ $country }}
                    <small style="color: #999;">
                        {{ count($in_country) }}
                        @php 
                            if(count($in_country) > 10 && count($in_country) < 15){
                                echo ' имен';
                            } else {
                                switch(count($in_country)-floor(count($in_country)/10)*10){
                                    case 1:
                                        echo ' имя';
                                        break;
                                    case 2:
                                    case 3:
                                    case 4:
                                        echo ' имени';
                                        break;
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 0:
                                        echo ' имен';
                                        break;
                                }
                            }
                        @endphp
                    </small>
                </h2>

                @foreach ($in_country as $i=>$line)
                    <div class="row text-left">
                        <div class="item col-12">
                            <p>
                                @if($line->photo)
                                    <img src="{{ $line->photo_preview }}" class="sinodik-photo mr-md-3 float-left">
                                @endif
                                {{ $i+1 }}. 
                                <b>
                                    {{ $line->san }}
                                    {{ $line->name }}
                                    {{ $line->lastname }}
                                </b>

                                @if($line->age)
                                    ({{ $line->age_str }})
                                @endif

                                {{ $line->reason }}

                                <span class="deceased">
                                    +{{ $line->deceased }}
                                </span>

                                <br>
                                @spaceless
                                    @if($line->country){{ $line->country }}@endif{{""}}@if($line->e), {{ $line->e }} епархия@endif{{""}}@if($line->town), {{ $line->town }}@endif{{""}}@if($line->place), {{ $line->place }}@endif{{""}}@if($line->role)<i>, {{ $line->role }}</i>@endif
                                @endspaceless

                                @if(strlen($line->comment) > 0)
                                    <br>
                                    <small>
                                        {{ $line->comment }}
                                    </small>
                                @endif

                                @if($line->photo)
                                    <br style="clear: both;">
                                @endif
                            </p>
                        </div>
                    </div>
                @endforeach


            @endforeach

        </div><!--//container-->
    </section><!--//benefits-section-->
       
    <!-- Javascript -->          
    <script src="/assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="/assets/plugins/popper.min.js"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
    <script src="/assets/plugins/jquery.scrollTo.min.js"></script>  
    <script src="/assets/plugins/back-to-top.js"></script>  
    
    <script src="/assets/js/main.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-98457204-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-98457204-3');
    </script>

</body>
</html>