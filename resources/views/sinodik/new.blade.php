<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Meta -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Content -->
	<title>Проект «Соборность»: Синодик почивших клириков в 2020-2022 г.</title>
	<meta name="description" content="Имена усопших для поминовения. Россия, Украина, Беларусь и другие страны">
	<meta name="author" content="Проект Соборность">    
	<meta property="og:title" content="Синодик умерших от ковида и иных причин" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="https://sobornost.ru/sinodik" />
	<meta property="og:image" content="https://sobornost.ru/assets/images/covers/sinodik_fb_cover_2.jpg" />
	<meta property="og:site_name" content="Синодик умерших от ковида и иных причин" />
	<!-- /Content -->

	<meta name="google-site-verification" content="ikoQ_xrqo3RLlUl4ZUvRf6VuQFtOr5pOQozuleBzLEQ" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="/assets/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:700|Roboto:400,400i,700&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
	
	<!-- FontAwesome JS-->
	<script defer src="/assets/fontawesome/js/all.min.js"></script>

	<!-- Theme CSS -->  
	<link id="theme-style" rel="stylesheet" href="/assets/css/theme.css">
	<link id="theme-style" rel="stylesheet" href="/assets/css/custom.css?v=2.3.4">

<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-516152-3kRh6"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-516152-3kRh6" style="position:fixed; left:-999px;" alt=""/></noscript>

</head> 

<body>
	<div id="sizer">
	    <div class="d-block d-sm-none d-md-none d-lg-none d-xl-none" data-size="xs"></div>
	    <div class="d-none d-sm-block d-md-none d-lg-none d-xl-none" data-size="sm"></div>
	    <div class="d-none d-sm-none d-md-block d-lg-none d-xl-none" data-size="md"></div>
	    <div class="d-none d-sm-none d-md-none d-lg-block d-xl-none" data-size="lg"></div>
	    <div class="d-none d-sm-none d-md-none d-lg-none d-xl-block" data-size="xl"></div>
	</div>
	<div class="corner-ribbon top-right sticky black d-none d-lg-block" style="font-size: 16px; line-height: 20px; padding: 5px 30px; z-index: 100;">
		Обновлен:<br>{{ $sinodik['last_update'] }}
	</div>
	<div style="position: fixed;top: 0px;left: 0px;width: 100%;text-align: center;background-color: #e0922e;box-shadow: 0px 3px 20px 0px #e0922e;padding: 5px 0px;color: white;z-index: 98;">
		Материалы семинара "Системные проблемы православия: анализ, осмысление, поиск решений".
		<a href="javascript:;" target="_blank" id="direct-download-1" style="font-size: 15px; color: white; border-bottom: 1px dashed;">Выпуск №1</a>
		|
		<a href="javascript:;" target="_blank" id="direct-download-2" style="font-size: 15px; color: white; border-bottom: 1px dashed;">Выпуск №2</a>
	</div>

	<header class="header">        
		<div class="branding">
			<div class="container-fluid position-relative">
				<div class="logo-wrapper">
					<div class="site-logo">
						<a class="navbar-brand m-0 p-0" href="/">
							<img class="logo-icon" src="/assets/images/site-logo.svg" alt="logo" style="height:210px; ">
						</a>
					</div>    
				</div><!--//docs-logo-wrapper-->
			
			</div><!--//container-->
		</div><!--//branding-->
	</header><!--//header-->

	<section id="description-section" class="benefits-section theme-bg-light-gradient py-md-5">
		<div class="container py-3">
			<h2 class="section-heading text-center mb-3">СИНОДИК</h2>
			<h5 class="text-center mb-3">почивших от коронавирусной инфекции и других причин епископов, церковно- и священнослужителей, 
			монашествующих и некоторых мирян с краткими комментариями</h5>

			<div class="d-lg-none d-xl-none text-center mb-3">
				Обновлен: {{ $sinodik['last_update'] }}
			</div>

			<div class="row mb-5">
				<div class="section-intro col-md-6 mx-auto">
					К сожалению, полноценной официальной статистики о зараженных и умерших церковно- и священнослужителях нет, 
					редакторы-добровольцы проекта "Соборность" с мая 2020 года и до сегодняшнего дня ведут и уточняют синодик. 
					Спасибо всем, кто принял участие в работе над ним. 
				</div>
				<div class="section-intro col-md-6 mx-auto">
					Если вы можете уточнить или дополнить наш синодик, пишите на <a href="mailto:sobornost.ru@gmail.com">sobornost.ru@gmail.com</a> 
					или на нашу страницу в фб <a href="https://www.facebook.com/sobornost.project" style="overflow-wrap: break-word;" target="_blank">https://www.facebook.com/sobornost.project</a><br><br>
					Друзья, сделайте репост, пригласите друзей помянуть умерших в своих молитвах!
				</div>
			</div>

			<div id="sinodik-app">
				<sinodik />
			</div>

		</div><!--//container-->
	</section><!--//benefits-section-->
	   
	<!-- Javascript -->          
	<script src="/assets/plugins/jquery-3.4.1.min.js"></script>
	<script src="/assets/plugins/popper.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
	<script src="/assets/plugins/jquery.scrollTo.min.js"></script>  
	<script src="/assets/plugins/back-to-top.js"></script>  
	<script src="https://unpkg.com/vue@3.2.24/dist/vue.global.prod.js"></script>

	<script>
		@php
			echo 'var countries = '.json_encode(array_keys($sinodik['data'])).'; ';
			echo 'var total_titles = '.$sinodik['total'].'; ';
			echo 'var last_update = "'.$sinodik['last_update'].'"; ';
		@endphp
	</script>
	<script type="x-template" id="sinodik">
	<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">              
				<div class="modal-body p-0">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<img src="" class="imagepreview" style="width: 100%;" >
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			По времени: 
			<div class="dropdown mb-3">
  				<button 
  					class="btn btn-secondary dropdown-toggle" 
  					type="button" 
  					id="dropdownMenuButton" 
  					data-toggle="dropdown" 
  					aria-expanded="false" 
  					style="border-radius: 4px; padding: 7px 21px;"
  				>
  					@{{ selected_range.title }}
  				</button>
  				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  					<a class="dropdown-item" href="javascript:;" v-for="range in ranges" @click="setRange(range)">@{{ range.title }}</a>
  				</div>
  			</div>

			По причине: 
			<div class="dropdown mb-3">
  				<button 
  					class="btn btn-secondary dropdown-toggle" 
  					type="button" 
  					id="dropdownMenuButton" 
  					data-toggle="dropdown" 
  					aria-expanded="false" 
  					style="border-radius: 4px; padding: 7px 21px;"
  				>
  					@{{ selected_covid.title }}
  				</button>
  				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  					<a class="dropdown-item" href="javascript:;" v-for="option in covid_options" @click="setCovid(option)">@{{ option.title }}</a>
  				</div>
  			</div>

			По странам: 
			<div class="dropdown mb-3">
  				<button 
  					class="btn btn-secondary dropdown-toggle" 
  					type="button" 
  					id="dropdownMenuButton" 
  					data-toggle="dropdown" 
  					aria-expanded="false" 
  					style="border-radius: 4px; padding: 7px 21px;"
  				>
  					@{{ countriesLabel() }}
  				</button>
  				<div class="dropdown-menu dropdown-country" aria-labelledby="dropdownMenuButton">
  					<a 
  						class="dropdown-item" 
  						href="javascript:;" 
  						@click="setCountry(country)" 
  						style="padding: .25rem 0.5rem" 
  						v-for="country, index in countries_dropdown"
  						:class="[{ 'checked': countryChecked(country) }]"
  						:key="'country-'+index"
  					>
  						<i class="fa fa-check"></i>
  						@{{ country }}
  					</a>
  				</div>
  			</div>
			<p>
				<b>Всего в синодике: {{ $sinodik['total'] }} имен,<br>из них показано @{{ total }}.</b>
			</p>
		</div>
		<div class="col-lg-8">
			<a href="javascript:;" @click="showStats" class="d-block d-lg-none">
				<template v-if="statsShown">
					Скрыть статистику по странам
				</template>
				<template v-else>
					Показать статистику по странам
				</template>
			</a>

			<div class="" v-show="statsShown">
				Статистика по странам: 
				<div class="row">
					<div class="item col-12 col-md-3">
						@php
							$c = 0;
						@endphp
						@foreach ($sinodik['data'] as $country=>$in_country)
							@php
								if($c == ceil(count($sinodik['data'])/4) || $c == ceil(count($sinodik['data'])/4)*2 || $c == ceil(count($sinodik['data'])/4)*3){
									echo '</div><div class="item col-12 col-md-3">';
								}
							@endphp
							<a href="javascript:;" @click="setCountry('{{ trim($country) }}', true)">
								{{ trim($country) }}
							</a> &dash; 

							{{ count($in_country) }}
							<br>

							@php
								$c++;
							@endphp
						@endforeach
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<a href="/sinodik/list" _target="_blank" class="btn btn-sm btn-outline-primary mt-3" style="border: 1px solid">Краткий список для поминовения</a>
				</div>
			</div>
		</div>
	</div>

	<div class="mt-3 alert alert-warning text-center" v-if="isLoading">
		<i class="fa fa-spin fa-spinner mr-4"></i> Загружаем информацию...
	</div>

	<section v-if="!zeroResult && !isLoading">
		<template v-for="country, in_country in data">

			<h2 class="section-heading text-center mt-4" :id="in_country">
				@{{ in_country }}
				<small style="color: #999;">
					@{{ country.length }} @{{ titleLang(country.length) }}
				</small>
			</h2>

			<template v-for="line, index in country">
				<article class="media mb-3">
					<div class="media-left" v-if="line.photo">
						<figure class="image is-100x100 pop" :src="line.photo">
							<img :src="line.photo_preview" class="sinodik-photo mr-md-3 float-left">
						</figure>
					</div>
					<div class="media-content my-3">
						<div class="content">
							@{{ index+1 }}.
							<b>
								@{{ line.san }}
								@{{ line.name }}
								@{{ line.lastname }}
							</b>

							<template v-if="line.age">
								(@{{ line.age_str }})
							</template>

							@{{ line.reason }}

							<span class="deceased">
								+@{{ line.deceased }}
							</span>

							<br>
							<template v-if="line.country">
								@{{ line.country }}
							</template>
							<template v-if="line.e">, @{{ line.e }} епархия</template>
							<template v-if="line.town">, @{{ line.town }}</template>
							<template v-if="line.place">, @{{ line.place }}</template>
							<template v-if="line.role"><i>, @{{ line.role }}</i></template>

	                        <template v-if="line.comment && line.comment.length > 0">
	                            <br>
	                            <small>
	                                @{{ line.comment }}
	                            </small>                        	
	                        </template>
						</div>
					</div>
				</article>
			</template>	
		</template>
	</section>
	<section v-else>
		<p class="alert alert-primary mt-3 text-center" v-if="!isLoading">
			По вашему запросу ничего не найдено. Пожалуйста, учтоните параметры поиска по времени, причине и странам.
		</p>
	</section>
	</script>

	<script src="/assets/js/main.js?v=2.3.4"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-98457204-3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-98457204-3');
	</script>

</body>
</html>