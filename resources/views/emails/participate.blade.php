@component('mail::message', ['mail' => $mail])
# Вот что пишут

<strong>Имя и фамилия:</strong> {{ $mail['participant_name'] }}<br>
<strong>Cан/Профессия:</strong> {{ $mail['participant_occupation'] }}<br>
<strong>Город/Регион:</strong> {{ $mail['participant_location'] }}<br>
<strong>Email:</strong> <a href="mailto:{{ $mail['participant_email'] }}">{{ $mail['participant_email'] }}</a><br>
<strong>Комментарий:</strong> {{ $mail['message'] }}<br>

Хорошего дня,<br>
{{ config('app.name') }}
@endcomponent