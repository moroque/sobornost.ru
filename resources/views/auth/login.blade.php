@extends('layouts.app')

@section('content')
<section class="hero is-primary is-fullheight">
	<div class="hero-body">
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-5-tablet is-4-desktop is-3-widescreen">
					<form method="POST" action="{{ route('login') }}" class="box">
						@csrf

						<div class="field">
							<label for="" class="label">{{ __('E-Mail Address') }}</label>
							<div class="control has-icons-left">
								<input id="email" type="email" class="input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
								<span class="icon is-small is-left">
									<i class="mdi mdi-account"></i>
								</span>
							</div>
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="field">
							<label for="" class="label">{{ __('Password') }}</label>
							<div class="control has-icons-left">
								<input id="password" type="password" class="input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
								<span class="icon is-small is-left">
									<i class="mdi mdi-lock"></i>
								</span>
							</div>
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror

						</div>
						<div class="field">
							<label for="" class="checkbox">
								<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
								{{ __('Remember Me') }}
							</label>
						</div>
						<div class="field">
							<button type="submit" class="button is-success">
								{{ __('Login') }}
							</button>

							@if (Route::has('password.request'))
								<a class="btn btn-link" href="{{ route('password.request') }}">
									{{ __('Forgot Your Password?') }}
								</a>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
