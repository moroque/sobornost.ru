<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Проект «Соборность»: системные проблемы Православия</title>

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Синодик умерших от ковида (обновляется)">
	<meta name="author" content="Sergey Chapnin">    
	<!-- Скачать PDF с материалами первого семинара -->
	<meta name="google-site-verification" content="ikoQ_xrqo3RLlUl4ZUvRf6VuQFtOr5pOQozuleBzLEQ" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="/assets/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<meta property="og:title" content="Проект «Соборность»: системные проблемы Православия" />
	<meta property="og:description" content="Системные проблемы православия: анализ, осмысление, поиск решений" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="https://sobornost.ru/" />
	<meta property="og:image" content="https://sobornost.ru/assets/images/covers/fb_2.jpg" />
	<meta property="og:site_name" content="Семинар Соборность" />

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:700|Roboto:400,400i,700&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
	
	<!-- FontAwesome JS-->
	<script defer src="assets/fontawesome/js/all.min.js"></script>

	<!-- Theme CSS -->  
	<link id="theme-style" rel="stylesheet" href="/assets/css/theme.css">
	<link id="theme-style" rel="stylesheet" href="/assets/css/custom.css">

	<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-516152-3kRh6"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-516152-3kRh6" style="position:fixed; left:-999px;" alt=""/></noscript>

</head>
<body>    
	<header class="header mt-md-4">	    
		<div class="branding">
			<div class="container-fluid position-relative py-3">
				<div class="logo-wrapper">
					<div class="site-logo">
						<a class="navbar-brand m-0 p-0" href="/">
							<img class="logo-icon" src="assets/images/site-logo.svg" alt="logo">
						</a>
						<p class="sinodik-badge">
							<a href="/sinodik/">
								Синодик умерших от ковида
							</a>
							<small>
								обновлен {{ $sinodik['last_update'] }}
							</small>
						</p>
					</div>    
				</div><!--//docs-logo-wrapper-->
			
			</div><!--//container-->
		</div><!--//branding-->
	</header><!--//header-->
	
	<section class="hero-section pb-5">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-7 pt-md-5 mb-5 align-self-center">
					<div class="promo pr-md-3 pr-lg-5">
						
						<h1 class="headline mb-3" style="font-size: 1.85rem;">
							Рабочие материалы<br>проектного семинара
							«Соборность»
						</h1><!--//headline-->
						<div class="subheadline mb-4">
							Приглашаем всех, кто размышляет о Церкви и историческом пути Православия, присоединиться к нашей дискуссии. Для начала – скачайте книгу и давайте ее обсудим. Это только начало разговора. Он обязательно будет продолжен.
						</div><!--//subheading-->
						
						<div class="row">
							<div class="col-6 text-center">
								<a class="btn btn-primary mb-2 d-inline-block scrollto" href="#form-section">Скачать выпуск №1</a>
								<br>
								<a class="scrollto" href="#content-section-1">Содержание</a>
							</div>
							<div class="col-6 text-center">
								<a class="btn btn-primary mb-2 d-inline-block scrollto" href="#form-section">Скачать выпуск №2</a>
								<br>
								<a class="scrollto" href="#content-section-2">Содержание</a>
							</div>
						</div>

						<div class="hero-quotes mt-3">
							<div id="quotes-carousel" class="quotes-carousel carousel slide carousel-fade mb-5" data-ride="carousel" data-interval="8000">
								<ol class="carousel-indicators">
									@foreach($testimonials as $i=>$t)
									<li data-target="#quotes-carousel" data-slide-to="{{ $i }}" class="@if($i == 0) active @endif"></li>
									@endforeach
								</ol>

								<a href="#reviews-section" style="position: absolute; bottom: -3.4rem;">Все отзывы</a>
							  
								<div class="carousel-inner">

									@foreach($testimonials as $i=>$t)
										<div class="carousel-item @if($i == 0) active @endif">
											<blockquote class="quote p-4 theme-bg-light">
												{!! $t->excerpt !!}
												<a href="javascript:;" class="ml-1" data-toggle="modal" data-target="#reviewModal{{ $t->id }}">Читать полностью</a>    
											</blockquote><!--//item-->
											<div class="source media flex-column flex-md-row align-items-center">
												@if($t->photo_preview)
													<img class="source-profile mr-md-3" src="{{ $t->photo_preview }}" alt="image" style="border-radius: 50%">
												@endif
												<div class="source-info media-body text-center text-md-left">
													<div class="source-name">
														{{ $t->person }}
													</div>
													<div class="soure-title">
														{{ $t->person_title }}
													</div>
												</div>
											</div><!--//source-->
										</div><!--//carousel-item-->							    	
									@endforeach

								</div><!--//carousel-inner-->
							</div><!--//quotes-carousel-->
							
						</div><!--//hero-quotes-->


					</div><!--//promo-->
				</div><!--col-->
				<div class="col-12 col-md-5 mb-5 align-self-center">
					<div class="book-cover-holder">
						
						<img class="img-fluid book-cover" src="assets/images/mock_2_1_res.png" alt="book cover" >
						<div class="book-badge d-inline-block shadow" style="line-height: 1.5rem;font-size: 1rem;width: 100px;height: 100px;padding-top: 30px;background: #3939b7;">
							<a href="#form-section" style="color: white;">
								Выпуск<br>№1
							</a>
						</div>
						<div class="book-badge d-inline-block shadow" style="right: 7rem; line-height: 2rem;">
							<a href="#form-section" style="color: white;">
								Выпуск<br>№2
							</a>
						</div>
					</div><!--//book-cover-holder-->

					<div class="author-section pt-5" style="position: absolute; width: 100%;">
						<span class="my-2 mx-2 float-left" style="color: #7d7d7d;">Расскажите о нашем семинаре в соцсетях: </span>
						<ul class="social-list list-unstyled">
							<li class="list-inline-item text-center">
								<a href="http://twitter.com/share?text=Системные проблемы православия: Анализ, осмысление, поиск решений&url=https://sobornost.ru" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<li class="list-inline-item text-center">
								<a href="https://www.facebook.com/sharer/sharer.php?u=https://sobornost.ru" target="_blank">
									<i class="fab fa-facebook"></i>
								</a>
							</li> 
							<li class="list-inline-item text-center">
								<a href="https://vk.com/share.php?url=https://sobornost.ru" target="_blank">
									<i class="fab fa-vk"></i>
								</a>
							</li>
							
						</ul><!--//social-list-->
					</div>
				</div><!--col-->
			</div><!--//row-->
		</div><!--//container-->
	</section><!--//hero-section-->
	
	<section id="description-section" class="benefits-section theme-bg-light-gradient py-5">
		<div class="container py-5">
			<h2 class="section-heading text-center mb-3">Системные проблемы православия:<br>анализ, осмысление, поиск решений</h2>
			<div class="section-intro single-col-max mx-auto text-center mb-5">Идея нашего семинара возникла в период пандемии covid-19. Однако из этого не следует, что в это время перед Церковью встали принципиально новые вопросы, прежде не возникавшие. <i>Нет ничего нового под солнцем (Эккл. 1, 9)</i>.</div>
			<div class="row text-left">
				<div class="item col-12 col-md-6">
					<p>Копившиеся многими десятилетиями <i>системные ошибки</i> не только стали очевидны и широко обсуждаются в церковных кругах и публичном пространстве, но и создают существенные угрозы самой церковной жизни.</p>
					<p>Пандемия лишь побудила более творчески и активно искать ответы на вопросы, которые и прежде стояли перед Церковью. Но если прежде можно было не замечать многие проблемы, то сегодня ситуация изменилась.</p>
					<p>Описать и проанализировать эти ошибки мы попытались в материалах нашего семинара. Однако мы убеждены, что ограничиться этим было бы недостаточно.</p>
					<p>Помимо этических проблем, связанных с ответственностью за заражение и смерть клириков и мирян, пандемия побудила к тому, чтобы сформулировать целый ряд вопросов общецерковного характера – богословских, литургических, канонических, пастырских, административных, финансово-экономических.</p>
				</div><!--//item-->
				<div class="item col-12 col-md-6">
					<p>Реагировать на происходящее в условиях пандемии приходилось быстро и решительно, принимая личную ответственность за неординарные и порой рискованные решения. </p>
					<p>Быть может, поэтому поиск ответов приводил к неожиданным и порой немыслимым еще несколько месяцев назад практикам: широкому распространению интернет-богослужений, раздаче Св. Даров мирянам для самостоятельного причащения дома, проведению исповеди по телефону, временной отмене церковного налога, сбору пожертвований исключительно банковскими переводами и другим.</p>
					<p>От произошедшего за эти месяцы невозможно будет отмахнуться и после окончания пандемии. Глубокую взаимосвязь названных выше и многих иных вопросов между собой нам еще предстоит осознать. Церковной Полноте, получившей столь масштабный новый опыт, с неизбежностью придется формулировать новое ви́дение Церкви.</p>
					<p></p>
					<p></p>
				</div><!--//item-->
			</div><!--//row-->
			<div class="section-intro mx-auto text-center mb-3">Эта новизна касается сложной и порой болезненной проблематики:</div>
			<div class="row">
				<div class="col-12">
					<div class="key-points mb-4 text-center">
						<ul class="key-points-list list-unstyled mb-4 mx-auto d-inline-block text-left">
							<li class="mb-2"><i class="fas fa-check-circle mr-2"></i>соотношения традиции и современной жизни;</li>
							<li class="mb-2"><i class="fas fa-check-circle mr-2"></i>понимания места Евхаристии и храмового богослужения в жизни христианина;</li>
							<li class="mb-2"><i class="fas fa-check-circle mr-2"></i>роли и места общины внутри Русской Православной Церкви как административной системы;</li>
							<li class="mb-2"><i class="fas fa-check-circle mr-2"></i>профессионализации (и клерикализации) служения духовенства и епископата;</li>
							<li class="mb-2"><i class="fas fa-check-circle mr-2"></i>меры и характера влияния монастырей на жизнь мирян и приходских общин.</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="section-intro mx-auto text-center mb-3">Перед Церковью, а значит, перед всеми нами стоит ответственная и творческая задача: предложить пути преодоления нынешней проблемной ситуации, обеспечить полноценность церковной жизни в условиях социально-экономического кризиса и возможного возвращения карантинных мер, защитить приходские и монашеские общины, поддержать общественную активность православных христиан.</div>

			<h2 class="section-heading text-center mb-4">Необходимость дискуссии</h2>
			<div class="row text-left mb-5">
				<div class="item col-12 col-md-6">
					<p>Содержательные обсуждения и споры по различным вопросам осмысления и устроения церковной жизни не утихают как минимум 10-15 лет. Они идут в социальных сетях, СМИ, на конференциях, семинарах и неформальных встречах. Во многих аналитических и публицистических текстах звучат разумные и порой детальные описания существующих и возможных практик, которые могут стать отправной точкой не только для формирования новых веток дискуссий,</p>
				</div>
				<div class="item col-12 col-md-6">
					<p>но и для создания новых документов, программ, канонических правил, развития литургических практик и пастырского попечения.</p>
					<p>Сегодня этот уникальный материал остается фрагментарным и несистематизированным. Его необходимо осмыслять и вводить в оборот, делать предметом широкой общецерковной дискуссии. Только тогда Церковь сможет этим воспользоваться – и сейчас, и в будущем.</p>
				</div>
			</div>

			<h2 class="section-heading text-center mb-4">Задачи семинара</h2>
			<div class="row text-left">
				<div class="item col-12 col-md-6">
					<p>Задачи проектного семинара «Системные проблемы Русского Православия: анализ, осмысление, поиск решений» лежат как в теоретической, так и в практической плоскости.</p>
					<p>Для участия в работе мы на первом этапе формируем круг экспертов. Этот список – открытый,  к нам можно присоединиться. Контакты на последней странице сборника.</p>
				</div>
				<div class="item col-12 col-md-6">
					<p class="mb-1">У нас три основные задачи:</p>
					<ul>
						<li>описание и систематизация программ, дискуссий и конкретных предложений, сформулированных за последние 10-15 лет;</li>
						<li>формирование положительной программы развития церковной жизни;</li>
						<li>популяризация и обсуждение этой программы в Русской Православной Церкви.</li>
					</ul>
				</div>
			</div>
			<div class="text-center" id="join-us">
				<button type="button" class="btn btn-secondary mt-4" data-toggle="modal" data-target="#participateModal">
					Присоединиться к семинару
				</button>
			</div>
		</div><!--//container-->
	</section><!--//benefits-section-->

	<section id="audience-section" class="audience-section theme-bg-light-gradient pt-md-5 pb-0">
		<div class="container">
			<h2 class="section-heading text-center mb-4">Наши принципы</h2>
			<div class="section-intro single-col-max mx-auto text-center mb-5">
				Кто мы? Какую позицию занимают участники семинара?<br>
				Прежде всего, мы очень разные. Среди нас есть миряне, монахи и клирики; богословы, историки и журналисты; гражданские активисты и предприниматели.<br>
				Нас объединяет несколько базовых принципов.
			</div><!--//section-intro-->
			<div class="audience mx-auto">
				<div class="item media">
					<div class="item-icon mr-3"><i class="fas fa-user-check"></i></div>
					<div class="media-body">
						<h4 class="item-title">Мы с уважением относимся к церковной иерархии и клиру</h4>
						<span class="item-desc">видим и ценим добрые дела, совершаемые теми, кто трудится на ниве Христовой.</span>
					</div><!--//media-body-->
				</div><!--//item-->
				<div class="item media">
					<div class="item-icon mr-3"><i class="fas fa-user-check"></i></div>
					<div class="media-body">
						<h4 class="item-title">Мы не ищем врагов ни внешних, ни внутренних</h4>
						<div class="item-desc">Заповедь о любви универсальна и распространяется на всех, в первую очередь – на несогласных с нами.</div>
					</div><!--//media-body-->
				</div><!--//item-->
				<div class="item media">
					<div class="item-icon mr-3"><i class="fas fa-user-check"></i></div>
					<div class="media-body">
						<h4 class="item-title">Мы предпочитаем видеть реальность во всей ее полноте.</h4>
						<div class="item-desc">Ответственное отношение к исторической Церкви как сообществу верных предполагает трезвый (критический) анализ происходящего и положительную программу действий, не связанную ни с какими корпоративными интересами и не ограниченную корпоративной моралью.</div>
					</div><!--//media-body-->
				</div><!--//item-->
				<div class="item media">
					<div class="item-icon mr-3"><i class="fas fa-user-check"></i></div>
					<div class="media-body">
						<h4 class="item-title">Мы убеждены, что не существует неудобных вопросов.</h4>
						<div class="item-desc">Обсуждать можно и нужно любые проблемы, если они поставлены прямо и вопрошающий готов услышать ответ. Попытки уклониться от ответа – это или признак слабости и вины, или желание манипулировать собеседником.</div>
					</div><!--//media-body-->
				</div><!--//item-->
				<div class="item media">
					<div class="item-icon mr-3"><i class="fas fa-user-check"></i></div>
					<div class="media-body">
						<h4 class="item-title">Мы руководствуемся тем, что во всем и ко всему должен быть творческий подход.</h4>
						<div class="item-desc">Слишком часто православные христиане следуют букве в ущерб Духу и называют это традицией. Так Традиция, утверждающая жизнь во Христе, становится оковами, а нередко и оправданием слов и поступков, несовместимых с образом Христа и Его заповедями.</div>
					</div><!--//media-body-->
				</div><!--//item-->
				<div class="item media">
					<div class="item-icon mr-3"><i class="fas fa-user-check"></i></div>
					<div class="media-body">
						<h4 class="item-title">Мы считаем, что каждый имеет право на ошибку.</h4>
						<div class="item-desc">При этом страх перед ошибкой не должен убивать свободную мысль и волю к действию, не должен лишать нас спокойного и трезвого внимания к окружающей реальности и последствиям собственных решений.</div>
					</div><!--//media-body-->
				</div><!--//item-->
			</div><!--//audience-->

			<div class="section-intro single-col-max mx-auto text-center mb-0">
				Это открытый список, и мы его будем дополнять.
			</div>
		</div><!--//container-->
	</section><!--//audience-section-->

	<section id="description-more-section" class="benefits-section py-5">
		<div class="container py-0">
			<div class="row text-left">
				<div class="item col-12 col-md-6">
					<p>Наша работа не направлена на борьбу или противостояние. Мы признаем сложившийся в нашей Церкви порядок вещей. Мы видим, что каждая из эпох в сложной и зачастую трагической истории нашей Церкви, включая ХХ столетие и нынешнее время, приносила свой вклад в копилку церковного опыта. Мы благодарны Богу за этот опыт. При этом мы не можем не видеть, что каждая из эпох приносила и вопросы, требующие ответа, и проблемы, настоятельно взывающие о своем разрешении.</p>
					<p>Сегодня мы видим немало мер, призванных решить конкретные задачи: укрепить «вертикаль власти», способствовать собираемости церковного налога, создать «положительный образ Церкви» и т.п. К сожалению, такой подход не работает. Это показали, в частности, церковно-административные реформы последнего десятилетия. Поэтому своей стратегической целью мы ставим создание целостной программы, одновременно смелой и реалистичной, дерзновенной и трезвой.</p>
					<p>Но прежде чем перейти к составлению подобного документа, важно сформулировать принципы, полагаемые в основание необходимых перемен, увидеть все пути и возможности дальнейшего развития. Это неизбежно потребует сначала экспертной, а потом и общецерковной дискуссии.</p>
				</div>
				<div class="item col-12 col-md-6">
					<p>Именно поэтому участники семинара предлагают различные, порой даже прямо противоположные подходы, далеко не всегда и не во всем соглашаясь друг с другом. На первом этапе цель проектного семинара «Соборность» состоит не в том, чтобы предложить стройную непротиворечивую программу. Важно обозначить возможно более полным образом пространство суждений, ориентированных на честное и глубокое осмысление существующих проблем и на их действительное разрешение.</p>
					<p>Стоит добавить, что среди нас есть те, кто подписывает свои тексты, и предпочитающие в нынешней ситуации по очевидным причинам выступать анонимно.</p>
					<p>Работа предстоит долгая и напряженная. Материалы семинара, которые мы представляем вашему вниманию, это лишь первый шаг. Мы проведем их широкое обсуждение и по его итогам составим доклад, развив и уточнив наши позиции на основе высказанных в обсуждении замечаний и мнений.</p>
					<p>Смиренно надеемся, что наш труд – вместе с трудами многих православных христиан, живущих в наши дни и болеющих сердцем о Церкви – станет еще одним христианским свидетельством, вдохновляемым проповедью апостола Павла о слове Божием, которое живо и действенно и острее всякого меча обоюдоострого (Евр, 4, 12).</p>
				</div>
			</div>

				<div class="row pt-4">
					<div class="col-md-12 text-md-center">
						<a class="btn btn-primary mr-5 mb-5 scrollto" href="#form-section">Получить PDF-файл выпуска №1</a>
						<a class="btn btn-primary mb-5 scrollto" href="#form-section">Получить PDF-файл выпуска №2</a>
					</div>
				</div>
		</div>
	</section>
	
	<section id="author-section" class="author-section section theme-bg-primary py-5 mb-5">
		<div class="container py-3">
			<div class="author-profile text-center mb-5">
				<img class="author-pic" src="assets/images/chapnin.jpg" alt="image" >
			</div><!--//author-profile-->
			<h2 class="text-center text-white mb-3">Сергей Чапнин</h2>
			<div class="author-bio single-col-max mx-auto">
				<div class="author-links text-center pt-0">
					<h5 class="text-white mb-4">Руководитель семинара</h5>
					<ul class="social-list list-unstyled">
						<li class="list-inline-item"><a href="https://www.facebook.com/chapnin" target="_blank"><i class="fab fa-facebook"></i></a></li> 
						<li class="list-inline-item"><a href="mailto:sobornost.ru@gmail.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
						<li class="list-inline-item"><a href="http://artos.org/" target="_blank"><i class="fas fa-globe-europe"></i></a></li>
					</ul><!--//social-list-->
				</div><!--//author-links-->
				
			</div><!--//author-bio-->
			
		</div><!--//container-->
	</section><!--//author-section-->

	<section id="content-section-1" class="content-section">
		<div class="container">
			<div class="col-md-11 mx-auto">
				<h2 class="section-heading text-center mb-5">Содержание первого выпуска</h2>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="key-points mb-4 text-center">
							<ul class="key-points-list list-unstyled mb-4 mx-auto d-inline-block text-left">
								<li><i class="fas fa-check-circle mr-2"></i>Введение</li>
								<li><i class="fas fa-check-circle mr-2"></i>Необходимость дискуссии</li>
								<li><i class="fas fa-check-circle mr-2"></i>Задачи семинара</li>
								<li><i class="fas fa-check-circle mr-2"></i>Наши принципы и стратегические намерения</li>
								<li><i class="fas fa-check-circle mr-2"></i>Проблема «темного двойника»  в контексте православной экклезиологии</li>
								<li><i class="fas fa-check-circle mr-2"></i>Молитва Церкви в период пандемии: новое осмысление реальности</li>
								<li><i class="fas fa-check-circle mr-2"></i>Закрытие храмов. Причины, значение, последствия</li>
								<li><i class="fas fa-check-circle mr-2"></i>Практики причащения: «богословие ложки»</li>
								<li><i class="fas fa-check-circle mr-2"></i>Интернет-трансляция меняет представление о литургии</li>
								<li><i class="fas fa-check-circle mr-2"></i>«Евхаристия онлайн»: богословские аспекты</li>
							</ul>
						</div>
					</div><!--//col-->
					<div class="col-12 col-md-6 mb-5">
						<div class="key-points mb-4 text-center">
							<ul class="key-points-list list-unstyled mb-4 mx-auto d-inline-block text-left">
								<li><i class="fas fa-check-circle mr-2"></i>Модернизация языка богослужения: возможны варианты</li>
								<li><i class="fas fa-check-circle mr-2"></i>Понятное богослужение. Еще раз о русском языке</li>
								<li><i class="fas fa-check-circle mr-2"></i>Пастырские практики: духовничество, старчество и младостарчество</li>
								<li><i class="fas fa-check-circle mr-2"></i>Православный фундаментализм</li>
								<li><i class="fas fa-check-circle mr-2"></i>Церковные финансы и налоги</li>
								<li><i class="fas fa-check-circle mr-2"></i>Публичный образ Церкви. Доверие общества и отношения с государством</li>
								<li><i class="fas fa-check-circle mr-2"></i>Поиск общин вне Московского Патриархата. Есть ли альтернатива?</li>
								<li><i class="fas fa-check-circle mr-2"></i>Разговор о будущем Церкви. Содержательность, проектность, самоопределение</li>
							</ul>
						</div><!--//key-points-->
						
					</div><!--//col-12-->   
				</div><!--//row-->
			</div><!--//single-col-max-->
		</div><!--//container-->
	</section><!--//content-section-->

	<section id="content-section-2" class="content-section">
		<div class="container">
			<div class="col-md-11 mx-auto">
				<h2 class="section-heading text-center mb-5">Содержание второго выпуска</h2>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="key-points mb-4 text-center">
							<ul class="key-points-list list-unstyled mb-4 mx-auto d-inline-block text-left">
								<li><i class="fas fa-check-circle mr-2"></i>Предисловие. Несколько слов о «церковном либерализме»</li>
								<li><i class="fas fa-check-circle mr-2"></i>Социологический опрос «Церковные практики в период пандемии: участие в богослужении, причащение, использование русского языка, финансовая поддержка прихода»</li>
								<li><i class="fas fa-check-circle mr-2"></i>Язык описания религиозного опыта как проблема современной церковной педагогики</li>
								<li><i class="fas fa-check-circle mr-2"></i>Искажение традиции или законное историческое развитие?</li>
								<li><i class="fas fa-check-circle mr-2"></i>Дисциплина поста: теория и практика</li>
							</ul>
						</div>
					</div><!--//col-->
					<div class="col-12 col-md-6 mb-5">
						<div class="key-points mb-4 text-center">
							<ul class="key-points-list list-unstyled mb-4 mx-auto d-inline-block text-left">
								<li><i class="fas fa-check-circle mr-2"></i>Суд или квазисуд? О перспективах церковного суда в современной России</li>
								<li><i class="fas fa-check-circle mr-2"></i>Политический кризис в Беларуси: православные христиане в общенациональном гражданском движении</li>
								<li><i class="fas fa-check-circle mr-2"></i>Приход и приходская жизнь. Опыт Церкви Англии</li>
								<li><i class="fas fa-check-circle mr-2"></i>Критерии фундаментализма в Русском Православии</li>
								<li><i class="fas fa-check-circle mr-2"></i>Западный обряд в современной Православной Церкви</li>
								<li><i class="fas fa-check-circle mr-2"></i>Внутрицерковная сообщительность: от «что происходит» к «что делать»</li>
							</ul>
						</div><!--//key-points-->
						
					</div><!--//col-12-->   
				</div><!--//row-->
			</div><!--//single-col-max-->
		</div><!--//container-->
	</section><!--//content-section-->

	<section id="form-section" class="form-section">
		<div class="container">
			<div class="lead-form-wrapper single-col-max mx-auto theme-bg-light rounded p-md-5">
				<h2 class="form-heading text-center">
					Получить PDF-файл книги
				</h2>
				<div class="form-intro text-center mb-3">
					Проект "Соборность" - это церковно-общественная инициатива, и мы нуждаемся в вашей помощи. Благодаря вашим пожертвованиям мы выпустили два сборника статей. Поддержите нас, отправьте пожертвование на издание третьего сборника.
				</div>
				<div class="row">
					<div class="col-md-6">
						<h5 class="text-center">Яндекс.Деньги</h5>
						<iframe src="https://yoomoney.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5&targets-hint=&default-sum=500&button-text=14&payment-type-choice=on&hint=%D0%AD%D1%82%D0%BE%20%D0%BF%D0%BE%20%D0%B6%D0%B5%D0%BB%D0%B0%D0%BD%D0%B8%D1%8E&successURL=http%3A%2F%2Fsobornost.ru&quickpay=shop&account=4100116566637228" width="323" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
					</div>
					<div class="col-md-6">
						<h5 class="text-center">Paypal</h5>
						<div id="smart-button-container" style="background: white; border: 1px solid #ddd; padding: 20px 20px 0px 20px;">
							<div style="text-align: center">
								<label for="description" style="font-size: 13px; color: #999; display: block;">Назначение</label>
								<input type="hidden" name="descriptionInput" id="description" maxlength="127" value="Пожертвование">
								<p class="mb-0">Пожертвование</p>
							</div>
							<p class="mb-0 d-none" id="descriptionError" style="visibility: hidden; color:red; text-align: center;">Please enter a description</p>
							<div style="text-align: center">
								<label style="font-size: 13px; color: #999; display: block;" for="amount">Сумма </label>
								<div class="input-group mb-3">
									<span class="input-group-text" style="border-radius: 5px 0px 0px 5px;">$</span>
									<input class="form-control" name="amountInput" type="number" id="amount" value="" style="height: 38px;" aria-label="Amount (to the nearest dollar)">
								</div>
							</div>
							<p class="mb-0 d-none" id="priceLabelError" style="visibility: hidden; color:red; text-align: center;">Please enter a price</p>
    						<div id="invoiceidDiv" style="text-align: center; display: none;"><label for="invoiceid"> </label><input name="invoiceid" maxlength="127" type="text" id="invoiceid" value="SOB-1" ></div>
      						<p class="mb-0 d-none" id="invoiceidError" style="visibility: hidden; color:red; text-align: center;">Please enter an Invoice ID</p>
    						<div style="text-align: center; margin-top: 0.625rem;" id="paypal-button-container"></div>
						</div>
					</div>
				</div>

				<div class="form-intro text-center mb-3 mt-3">
					Оставьте адрес своей электронной почты<br>и мы пришлем вам pdf-файл, а также<br>будем держать вас в курсе<br> главных новостей нашего проекта.<br> Это по вашему желанию.
				</div>
				<div class="form-wrapper">
					<!-- Begin Mailchimp Signup Form -->
					<div id="mc_embed_signup">
						<form action="https://sobornost.us2.list-manage.com/subscribe/post?u=46a30dc3a7f942930d414809c&amp;id=9d3e0fa027" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline justify-content-center flex-wrap" target="_blank" novalidate>
							<div id="mc_embed_signup_scroll">
								<div class="form-group mr-md-2 mb-3 mc-field-group">
									<label for="mce-EMAIL" class="sr-only">Email</label>
									<input type="email" name="EMAIL" class="form-control " id="mce-EMAIL" placeholder="Ваша электронная почта">
								</div>

								<div class="form-group mr-md-2 mb-3 mc-field-group">
									<label for="mce-FNAME" class="sr-only">Имя / First Name </label>
									<input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME" placeholder="Ваше имя">
								</div>
								<div class="form-group mr-md-2 mb-3 mc-field-group">
									<label for="mce-LNAME" class="sr-only">Фамилия / Last Name </label>
									<input type="text" value="" name="LNAME" class="form-control" id="mce-LNAME" placeholder="Ваша фамилия">
								</div>
								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_46a30dc3a7f942930d414809c_9d3e0fa027" tabindex="-1" value=""></div>
								<div class="clear">
									<input type="submit" value="Подписаться и получить оба выпуска" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary btn-submit mb-3 mb-md-0">
								</div>
							</div>
						</form>
					</div>
					<!--End mc_embed_signup-->

					<div class="mt-3 text-center">
						Просто скачать pdf-файл<br>
						<a href="javascript:;" target="_blank" id="direct-download-1" class="text-muted" style="font-size: 15px;">Выпуск №1</a>
						|
						<a href="javascript:;" target="_blank" id="direct-download-2" class="text-muted" style="font-size: 15px;">Выпуск №2</a>
					</div>
				</div><!--//form-wrapper-->
			</div><!--//lead-form-wrapper-->
		</div><!--//container-->
	</section><!--//form-section-->

	<section id="author-section" class="author-section section theme-bg-primary py-5 my-5">
		<div class="container py-3">
			<div class="row">
				<div class="col-md-4">
					<img src="https://artos.gallery/wp-content/uploads/2017/04/artos.org_.png" alt="image" style="width: 100%" >
				</div>
				<div class="col-md-8 text-center">
					<h2 class="text-white mb-3">Приобрести первый выпуск сборника<br>в печатном виде можно</h2>
					<a class="btn btn-lg btn-secondary" href="https://artos.gallery/gallery/books/sistemnye-problemy-pravoslaviya-01-2020/" target="_blank">
						в галерее Артос
						<i class="fa fa-external-link-alt ml-2"></i>
					</a>
				</div>
			</div>

			
		</div><!--//container-->
	</section><!--//author-section-->

	<section class="mt-5 d-none" id="support-us">
		<div class="container text-center">
			<button class="btn btn-lg btn-secondary" data-toggle="modal" data-target="#donateModal">Поддержите нас!</button>
		</div>
	</section>

	<section id="reviews-section" class="reviews-section py-5">
		<div class="container">
			<h2 class="section-heading text-center">Отзывы</h2>
			<div class="section-intro text-center single-col-max mx-auto mb-5">Читатели о нашем сборнике</div>

			<div class="row justify-content-center">

				@foreach($testimonials as $i=>$t)
					<div class="item col-12 col-lg-6 p-3 mb-4">
						<div class="item-inner theme-bg-light rounded p-4">
							<blockquote class="quote">
								{!! $t->excerpt !!}
								<a href="javascript:;" class="ml-1" data-toggle="modal" data-target="#reviewModal{{ $t->id }}" style="white-space: nowrap;">Читать полностью</a>    
							</blockquote><!--//item-->

							<div class="source media flex-column flex-md-row align-items-center">
								@if($t->photo_preview)
									<img class="source-profile mr-md-3" src="{{ $t->photo_preview }}" alt="image" style="border-radius: 50%">
								@endif
								<div class="source-info media-body text-center text-md-left">
									<div class="source-name">
										{{ $t->person }}
									</div>
									<div class="soure-title">
										{{ $t->person_title }}
									</div>
								</div>
							</div><!--//source-->
							<div class="icon-holder"><i class="fas fa-quote-right"></i></div>
						</div><!--//carousel-item-->
					</div>
				@endforeach

			</div>
		</div>
	</section>
	
	<footer class="footer">

		<div class="footer-bottom text-center py-5">

			<!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
			<small class="copyright">Адаптированный дизайн страницы: <a class="theme-link" href="http://themes.3rdwavemedia.com" target="_blank">3rdwavemedia</a></small>
 
		</div>
		
	</footer>

	<!-- Success Modal -->
	<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Спасибо за ваш интерес!</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Письмо со вложением направлено на указанный вами адрес электронной почты.</p>
					<p>Сейчас браузер предложит вам также сохранить файл.</p>
					<p>Обратите внимание, что ваш браузер мог заблокировать окно к согласием на подписку на нашу ненавязчивую рассылку.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Понятно</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Review1 Modal -->
	@foreach($testimonials as $i=>$t)
		<div class="modal fade reviewModal" id="reviewModal{{ $t->id }}" tabindex="-1" role="dialog" aria-labelledby="reviewModal{{ $t->id }}CenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">
							{{ $t->title }}
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="source media flex-column flex-md-row align-items-center">
							@if($t->photo_preview)
								<img class="source-profile mr-md-3  mb-3" src="{{ $t->photo }}" alt="image" style="width: 150px; border-radius: 50%">
							@endif
							<div class="source-info media-body text-center text-md-left">
								<div class="source-name">
									{{ $t->person }}
								</div>
								<div class="soure-title">
									{{ $t->person_title }}
								</div>
							</div>
						</div><!--//source-->
						{!! $t->text !!}
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
					</div>
				</div>
			</div>
		</div>
	@endforeach


	<!-- Donate Modal -->
	<div class="modal fade" id="donateModal" tabindex="-1" role="dialog" aria-labelledby="donateModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Нас можно поддержать!</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-intro text-center mb-3">
						Проект "Соборность" - это церковно-общественная инициатива, и мы нуждаемся в вашей помощи. Благодаря вашим пожертвованиям мы выпустили два сборника статей. Поддержите нас, отправьте пожертвование на издание третьего сборника.
					</div>
					<div class="row">
						<div class="col-md-6">
							<h5 class="text-center">Яндекс.Деньги</h5>
							<iframe src="https://yoomoney.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5&targets-hint=&default-sum=300&button-text=14&payment-type-choice=on&hint=%D0%AD%D1%82%D0%BE%20%D0%BF%D0%BE%20%D0%B6%D0%B5%D0%BB%D0%B0%D0%BD%D0%B8%D1%8E&successURL=http%3A%2F%2Fsobornost.ru&quickpay=shop&account=4100116566637228" width="323" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
						</div>
						<div class="col-md-6">
							<h5 class="text-center">Paypal</h5>
							<div id="smart-button-container-modal" style="background: white; border: 1px solid #ddd; padding: 20px 20px 0px 20px;">
								<div style="text-align: center">
									<label for="description" style="font-size: 13px; color: #999; display: block;">Назначение</label>
									<input type="hidden" name="descriptionInput" id="description" maxlength="127" value="Пожертвование">
									<p class="mb-0">Пожертвование</p>
								</div>
								<p class="mb-0 d-none" id="descriptionError" style="visibility: hidden; color:red; text-align: center;">Please enter a description</p>
								<div style="text-align: center">
									<label style="font-size: 13px; color: #999; display: block;" for="amount">Сумма </label>
									<div class="input-group mb-3">
										<span class="input-group-text" style="border-radius: 5px 0px 0px 5px;">$</span>
										<input class="form-control" name="amountInput" type="number" id="amount" value="" style="height: 38px;" aria-label="Amount (to the nearest dollar)">
									</div>
								</div>
								<p class="mb-0 d-none" id="priceLabelError" style="visibility: hidden; color:red; text-align: center;">Please enter a price</p>
	    						<div id="invoiceidDiv" style="text-align: center; display: none;"><label for="invoiceid"> </label><input name="invoiceid" maxlength="127" type="text" id="invoiceid" value="SOB-1" ></div>
	      						<p class="mb-0 d-none" id="invoiceidError" style="visibility: hidden; color:red; text-align: center;">Please enter an Invoice ID</p>
	    						<div style="text-align: center; margin-top: 0.625rem;" id="paypal-button-container-modal"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Хорошо</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Participate Modal -->
	<div class="modal fade" id="participateModal" tabindex="-1" role="dialog" aria-labelledby="participateModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Присоединиться к семинару</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="participate" method="post">
						<div class="form-group">
							<label for="participant-name" class="col-form-label">Ваше имя и фамилия:</label>
							<input type="text" class="form-control" required id="participant-name" name="participant_name">
						</div>
						<div class="form-group">
							<label for="participant-occupation" class="col-form-label">Cан/Профессия:</label>
							<input type="text" class="form-control" id="participant-occupation" name="participant_occupation">
						</div>
						<div class="form-group">
							<label for="participant-location" class="col-form-label">Город/Регион:</label>
							<input type="text" class="form-control" id="participant-location" name="participant_location">
						</div>
						<div class="form-group">
							<label for="participant-email" class="col-form-label">Адрес электронной почты:</label>
							<input type="email" class="form-control" id="participant-email" name="participant_email">
						</div>
						<div class="form-group">
							<label for="message-text" class="col-form-label">Комментарий (Как вы видите свое участие в семинаре? и т.п.):</label>
							<textarea class="form-control" id="message-text" name="message"></textarea>
						</div>

						<div class="text-muted" style="font-size: 12px;">
							Нажимая кнопку "Отправить", вы даете свое согласие руководителю некоммерческого проекта Соборность на хранение и обработку ваших персональных данных, указанных в полях формы. Указанные персональные данные могут быть использованы только для связи с вами, мы не передаем персональные данные третьим сторонам. 
						</div>
					</form>
					<div class="alert alert-danger" style="display: none;" role="alert" id="formError">
						Необходимо заполнить все поля
					</div>
					<div class="alert alert-success" style="display: none;" role="alert" id="formSuccess">
						Спасибо за вашу готовность участвовать в работе. В ближайшее время мы свяжемся с вами!
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
					<button type="button" class="btn btn-primary" id="participateSend">Отправить</button>
				</div>
			</div>
		</div>
	</div>
	   
	<!-- Javascript -->          
	<script src="assets/plugins/jquery-3.4.1.min.js"></script>
	<script src="assets/plugins/popper.min.js"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
	<script src="assets/plugins/jquery.scrollTo.min.js"></script>  
	<script src="assets/plugins/back-to-top.js"></script>  
	
	<script src="https://www.paypal.com/sdk/js?client-id=ATjDddl7dCfsvCHW68qRSS0SiX4WJURy2AZPmP9h2013Eb5TlNRf6KZFa17jn5etT08IkC1XVFcjc7wg&enable-funding=venmo&currency=USD" data-sdk-integration-source="button-factory"></script>
	<script src="assets/js/main.js"></script>

	<script>
		initPayPalButtonMain();
		//initPayPalButtonModal();
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-98457204-3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-98457204-3');
	</script>

</body>
</html>