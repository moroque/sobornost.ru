<?php

use Illuminate\Support\Facades\Route;
use App\Models\Sinodik;
use App\Models\Testimonial;
use Carbon\Carbon;
use App\Http\Controllers\SinodikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PAGES
Route::get('/', function () {
	$total = Sinodik::count();
	$last_update = Sinodik::orderBy('updated_at', 'desc')->first();
	if($last_update){
		$date = new Carbon($last_update->updated_at);
		$last_update = $date->format('d.m.Y');
	} else {
		$last_update = 'никогда';
	}

	$testimonials = Testimonial::wherePublished(true)->orderBy('updated_at', 'desc')->get();

	return view('seminar', [ 
		'sinodik' => [ 
			'total' => $total, 
			'last_update' => $last_update
		],
		'testimonials' => $testimonials
	]);
});
Route::get('/sinodik', [SinodikController::class, 'front']);
Route::get('/sinodik_new', [SinodikController::class, 'front_new']);
Route::get('/sinodik/russia/{covid?}', [SinodikController::class, 'russia']);
Route::get('/sinodik/list', [SinodikController::class, 'list']);

// DEV routes
Route::get('/setup_permissions', function(){
	$array = [
		'view downloads',
		'create downloads',
		'edit downloads',
		'delete downloads',

		'view testimonials',
		'create testimonials',
		'edit testimonials',
		'delete testimonials',
		'publish testimonials',
		'unpublish testimonials',

		'view sinodiks',
		'create sinodiks',
		'edit sinodiks',
		'delete sinodiks',
		'publish sinodiks',
		'unpublish sinodiks',

		'view users',
		'create users',
		'edit users',
		'delete users',
	];

	foreach($array as $perm){
		Spatie\Permission\Models\Permission::create(['name' => $perm]);
	}

	Spatie\Permission\Models\Role::create(['name' => 'Super Admin']);
});

// SUPPORT ROUTES
Route::get('/download/{part?}', function($part = 'all'){
	if($part == 1){
		$pathToFile = storage_path('app/pdf/pdf.pdf');
		$name = 'Семинар Соборность - Системные проблемы Православия (Выпуск №1).pdf';
		$headers = ['Content-Type: application/pdf'];
	} elseif($part == 2){
		$pathToFile = storage_path('app/pdf/pdf2021_1.pdf');
		$name = 'Семинар Соборность - Системные проблемы Православия (Выпуск №2).pdf';
		$headers = ['Content-Type: application/pdf'];
	} elseif($part == 'all') {
		$pathToFile = storage_path('app/pdf/1+2.zip');
		$name = 'Семинар Соборность - Системные проблемы Православия (Выпуски №1 и №2).zip';
		$headers = ['Content-Type: application/zip'];
	}
	
	return response()->download($pathToFile, $name, $headers);
});

Route::post('/dl', function(){
	$ip = Request::ip();

	$geo = json_decode(file_get_contents('http://api.ipstack.com/'.$ip.'?access_key=0bb5b0caed6b4922f515dd41bcbb0ae0&language=ru'), true);

	$new = [
		'email' => 'direct_download',
		'name' => Request::input('FNAME'),
		'lastname' => Request::input('LNAME'),
		'geo' => $geo,
		'country' => $geo['country_name'],
		'region' => $geo['region_name'],
		'city' => $geo['city'],
		'ip' => $ip
	];

	if(Request::input('EMAIL')){
		$new['email'] = Request::input('EMAIL');

		Mail::to(Request::input('EMAIL'))->send(new App\Mail\PdfDownload(Request::all()));
	}

	$record = App\Models\Download::create($new);

	return compact('new', 'record');
});

Route::post('/participate', function(){
	Mail::to(env('SBR_EMAIL'))->send(new App\Mail\Participate(Request::all()));
});

Route::post('/app/sinodik/public', [SinodikController::class, 'public_data']);
Route::post('/app/sinodik/list/public', [SinodikController::class, 'public_list_data']);

// ADMIN ROUTES
Route::group(['middleware' => 'auth:web'], function(){
	Route::get('/s/panel/stats', [App\Http\Controllers\HomeController::class, 'panel_stats']);

	Route::post('/app/downloads', [App\Http\Controllers\DownloadController::class, 'index']);
	Route::post('/app/sinodiks', [App\Http\Controllers\SinodikController::class, 'index']);
	Route::post('/app/testimonials', [App\Http\Controllers\TestimonialController::class, 'index']);
	Route::post('/app/users', [App\Http\Controllers\UserController::class, 'index']);
	Route::get('/app/user', [App\Http\Controllers\UserController::class, 'show']);

	Route::get('/s/sinodik/export', [App\Http\Controllers\SinodikController::class, 'export']);
	Route::post('/s/sinodik/add', [App\Http\Controllers\SinodikController::class, 'store']);
	Route::post('/s/sinodik/{sinodik}/update', [App\Http\Controllers\SinodikController::class, 'update']);
	Route::post('/s/sinodik/{sinodik}/photo', [App\Http\Controllers\SinodikController::class, 'upload_photo']);
	Route::delete('/s/sinodik/{sinodik}/delete', [App\Http\Controllers\SinodikController::class, 'destroy']);
	Route::delete('/s/sinodik/{sinodik}/delete_photo', [App\Http\Controllers\SinodikController::class, 'destroy_photo']);

	Route::post('/s/testimonials/add', [App\Http\Controllers\TestimonialController::class, 'store']);
	Route::post('/s/testimonials/{testimonial}/update', [App\Http\Controllers\TestimonialController::class, 'update']);
	Route::post('/s/testimonials/{testimonial}/photo', [App\Http\Controllers\TestimonialController::class, 'upload_photo']);
	Route::delete('/s/testimonials/{testimonial}/delete', [App\Http\Controllers\TestimonialController::class, 'destroy']);
	Route::delete('/s/testimonials/{testimonial}/delete_photo', [App\Http\Controllers\TestimonialController::class, 'destroy_photo']);

	Route::post('/s/users/add', [App\Http\Controllers\UserController::class, 'store']);
	Route::post('/s/users/{user}/update', [App\Http\Controllers\UserController::class, 'update']);
	Route::delete('/s/users/{user}/delete', [App\Http\Controllers\UserController::class, 'destroy']);
/*
	// TMP ROUTES
	Route::get('/s/import/downloads', function(){
		return true;
		$data = json_decode(Storage::get('database/db.json'), true);

		foreach($data as $line){
			$new = [
				'email' => $line['email'],
				'name' => '',
				'lastname' => '',
				'geo' => $line['geo'],
				'ip' => $line['geo']['ip'],
				'country' => $line['geo']['country_name'],
				'region' => $line['geo']['region_name'],
				'city' => $line['geo']['city']
			];

			$dl = App\Models\Download::create($new);

			$dl->created_at = $line['time'];
			$dl->save();
		}
	});

	Route::get('/s/import/sinodik', function(){
		return true;
		//$data = json_decode(Storage::get('database/sinodik.json'), true);

		$file = storage_path('app/database/sinodik.json');
		$data = array();
		foreach(file($file) as $row){
			$row = str_replace("\r\n", "", $row);
			$data[] = explode(';', $row);
		}

		foreach($data as $line){
			$date = explode('.', trim($line[4]));
			if(trim($line[4]) == ""){
				$deceased = null;
			} else {
				$deceased = (strlen(trim($line[4])) == 4 ? trim($line[4]).'-12-31' : $date[2].'-'.$date[1].'-'.$date[0]);
			}
			$new = [
				'san' => trim($line[0]),
				'name' => trim($line[1]),
				'lastname' => trim($line[2]),
				'age' => trim($line[3]),
				'deceased' => $deceased,
				'reason' => trim($line[5]),
				'country' => trim($line[6]),
				'e' => trim($line[7]),
				'town' => trim($line[8]),
				'place' => trim($line[9]),
				'role' => (isset($line[10]) ? trim($line[10]) : ''),
				'lock' => false
			];

			$s = App\Models\Sinodik::create($new);

		}
	});

	Route::get('/s/downloads/deduplicate', [App\Http\Controllers\DownloadController::class, 'deduplicate']);
*/
	Route::get('/s/{any?}', function(){
		return view('admin');
	})->where('any', '.*');
});

Auth::routes([
	'register' => false,
	'request' => false,
	'reset' => false
]);

//Route::get('/sinodik/edit', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
