<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSinodiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinodiks', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            
            $table->string('san')->nullable();
            	$table->string('san_rp')->nullable();
            $table->string('name')->nullable();
            	$table->string('name_rp')->nullable();
            $table->string('lastname')->nullable();
            	$table->string('lastname_rp')->nullable();
            $table->string('age')->nullable();
            $table->date('deceased')->nullable();
            $table->string('reason')->nullable();
            $table->string('country')->nullable();
            $table->string('e')->nullable();
            $table->string('town')->nullable();
            $table->string('place')->nullable();
            $table->string('role')->nullable();

            $table->boolean('lock');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinodiks');
    }
}
