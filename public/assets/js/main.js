$(document).ready(function() {
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
		}
	});
	
	/* ===== Smooth scrolling ====== */
	$('a.scrollto').on('click', function(e){
		//store hash
		var target = this.hash;    
		e.preventDefault();
		$('body').scrollTo(target, 800, {offset: 0, 'axis':'y'});

	});
	
	$('#mc-embedded-subscribe-form').on('submit', function(e){
		if($.trim($('#mce-EMAIL').val()) == ''){
			return false
		}

		$.post('/dl', $('#mc-embedded-subscribe-form').serializeArray(), function(response){

		});

		window.open('/download/all')
		//console.log('/download/all')

		$('#successModal').modal('toggle')
	});

	$('#direct-download-1').on('click', function(){
		$.post('/dl', $('#mc-embedded-subscribe-form').serializeArray(), function(response){

		});

		window.open('/download/1')

		$('#donateModal').modal('toggle')
	});

	$('#direct-download-2').on('click', function(){
		$.post('/dl', $('#mc-embedded-subscribe-form').serializeArray(), function(response){

		});

		window.open('/download/2')
		$('#donateModal').modal('toggle')
	});

	$('#participateSend').on('click', function(){
		$('#participateModal form').submit()
	})
	
	$('#participateModal form').on('submit', function(e){
		e.preventDefault()
		
		var data = $('#participateModal form').serializeArray(),
			formFilled = true

		$('#formSuccess').hide()
		$('#formError').hide()

		data.forEach(el => {
			if(el.value == ''){
				formFilled = false
			}
		})

		if(!formFilled){
			$('#formError').show()

			return false
		}

		$('#formSuccess').show()
		$(this).hide()

		$.post('/participate', data, function(response){

		})

		setTimeout(function(){
			$('#participateModal').modal('hide')
		}, 5000)
	})

	$('body').on('click', '.pop', function() {
		$('.imagepreview').attr('src', $(this).attr('src'));
		$('#imagemodal').modal('show');   
	});	
});

function initPayPalButtonMain() {
	var description = document.querySelector('#smart-button-container #description');
	var amount = document.querySelector('#smart-button-container #amount');
	var descriptionError = document.querySelector('#smart-button-container #descriptionError');
	var priceError = document.querySelector('#smart-button-container #priceLabelError');
	var invoiceid = document.querySelector('#smart-button-container #invoiceid');
	var invoiceidError = document.querySelector('#smart-button-container #invoiceidError');
	var invoiceidDiv = document.querySelector('#smart-button-container #invoiceidDiv');

	var elArr = [description, amount];

	if (invoiceidDiv.firstChild.innerHTML.length > 1) {
		invoiceidDiv.style.display = "block";
	}

	var purchase_units = [];
	purchase_units[0] = {};
	purchase_units[0].amount = {};

	function validate(event) {
		return event.value.length > 0;
	}

	paypal.Buttons({
		style: {
			color: 'gold',
			shape: 'rect',
			label: 'paypal',
			layout: 'vertical',
		},
		onInit: function (data, actions) {
			actions.disable();

			if(invoiceidDiv.style.display === "block") {
				elArr.push(invoiceid);
			}

			elArr.forEach(function (item) {
				item.addEventListener('keyup', function (event) {
					var result = elArr.every(validate);
					if (result) {
						actions.enable();
					} else {
						actions.disable();
					}
				});
			});
		},
		onClick: function () {
			if (description.value.length < 1) {
				descriptionError.style.visibility = "visible";
			} else {
				descriptionError.style.visibility = "hidden";
			}

			if (amount.value.length < 1) {
				priceError.style.visibility = "visible";
			} else {
				priceError.style.visibility = "hidden";
			}

			if (invoiceid.value.length < 1 && invoiceidDiv.style.display === "block") {
				invoiceidError.style.visibility = "visible";
			} else {
				invoiceidError.style.visibility = "hidden";
			}

			purchase_units[0].description = description.value;
			purchase_units[0].amount.value = amount.value;

			if(invoiceid.value !== '') {
				purchase_units[0].invoice_id = invoiceid.value;
			}
		},
		createOrder: function (data, actions) {
			return actions.order.create({
				purchase_units: purchase_units,
			});
		},
		onApprove: function (data, actions) {
			return actions.order.capture().then(function (details) {
				alert('Transaction completed by ' + details.payer.name.given_name + '!');
			});
		},
		onError: function (err) {
			console.log(err);
		}
	}).render('#paypal-button-container');
}

function initPayPalButtonModal() {
	var description = document.querySelector('#smart-button-container-modal #description');
	var amount = document.querySelector('#smart-button-container-modal #amount');
	var descriptionError = document.querySelector('#smart-button-container-modal #descriptionError');
	var priceError = document.querySelector('#smart-button-container-modal #priceLabelError');
	var invoiceid = document.querySelector('#smart-button-container-modal #invoiceid');
	var invoiceidError = document.querySelector('#smart-button-container-modal #invoiceidError');
	var invoiceidDiv = document.querySelector('#smart-button-container-modal #invoiceidDiv');

	var elArr = [description, amount];

	if (invoiceidDiv.firstChild.innerHTML.length > 1) {
		invoiceidDiv.style.display = "block";
	}

	var purchase_units = [];
	purchase_units[0] = {};
	purchase_units[0].amount = {};

	function validate(event) {
		return event.value.length > 0;
	}

	paypal.Buttons({
		style: {
			color: 'gold',
			shape: 'rect',
			label: 'paypal',
			layout: 'vertical',
		},
		onInit: function (data, actions) {
			actions.disable();

			if(invoiceidDiv.style.display === "block") {
				elArr.push(invoiceid);
			}

			elArr.forEach(function (item) {
				item.addEventListener('keyup', function (event) {
					var result = elArr.every(validate);
					if (result) {
						actions.enable();
					} else {
						actions.disable();
					}
				});
			});
		},
		onClick: function () {
			if (description.value.length < 1) {
				descriptionError.style.visibility = "visible";
			} else {
				descriptionError.style.visibility = "hidden";
			}

			if (amount.value.length < 1) {
				priceError.style.visibility = "visible";
			} else {
				priceError.style.visibility = "hidden";
			}

			if (invoiceid.value.length < 1 && invoiceidDiv.style.display === "block") {
				invoiceidError.style.visibility = "visible";
			} else {
				invoiceidError.style.visibility = "hidden";
			}

			purchase_units[0].description = description.value;
			purchase_units[0].amount.value = amount.value;

			if(invoiceid.value !== '') {
				purchase_units[0].invoice_id = invoiceid.value;
			}
		},
		createOrder: function (data, actions) {
			return actions.order.create({
				purchase_units: purchase_units,
			});
		},
		onApprove: function (data, actions) {
			return actions.order.capture().then(function (details) {
				alert('Transaction completed by ' + details.payer.name.given_name + '!');
			});
		},
		onError: function (err) {
			console.log(err);
		}
	}).render('#paypal-button-container-modal');
}

function viewSize() {
    return $('#sizer').find('div:visible').data('size');
}

Vue.createApp({}).component('sinodik', {
	template: '#sinodik',
	data(){
		return {
			ranges: [
				{ title: 'Последние 7 дней', value: 'seven_days' },
				{ title: 'Последние 40 дней', value: 'fourty_days' },
				{ title: 'Последние 3 месяца', value: 'three_months' },
				{ title: 'Последние 6 месяцев', value: 'six_months' },
				{ title: 'Последний год', value: 'one_year' },
				{ title: '2021 год', value: '2021_year' },
				{ title: '2020 год', value: '2020_year' },
				{ title: 'Все', value: 'all' }
			],
			selected_range: { title: 'Последние 6 месяцев', value: 'six_months' },
			covid_options: [
				{ title: 'Ковид', value: 'covid' },
				{ title: 'Не ковид', value: 'noncovid' },
				{ title: 'Все', value: 'all' },
			],
			selected_covid: { title: 'Все', value: 'all' },
			countries: window.countries.filter(value => value.length > 0),
			selected_countries: [],
			data: {},
			total: 0,
			isLoading: false,
			zeroResult: false,
			statsShown: (viewSize() == 'xl' ? true : false)
		}
	},
	computed:{
		countries_dropdown(){
			let mod = this.countries.filter(item => {
				return item != 'Россия' && item != 'Украина' && item != 'Беларусь'
			})

			mod.unshift('Беларусь')
			mod.unshift('Украина')
			mod.unshift('Россия')

			return mod
		}
	},
	mounted(){
		this.getData()
	},
	methods:{
		showStats(){
			this.statsShown = !this.statsShown
		},
		setRange(range){
			this.selected_range = range
			this.getData()
		},
		setCovid(option){
			this.selected_covid = option
			this.getData()
		},
		setCountry(country, single = false){
			if(this.selected_countries.indexOf(country) > -1){
				this.selected_countries.splice(this.selected_countries.indexOf(country), 1)
			} else {
				if(single){
					this.selected_countries = [country]
				} else {
					this.selected_countries.push(country)
				}
			}
			this.selected_countries.sort()

			this.getData()
		},
		countriesLabel(){
			if(this.selected_countries.length > 2){
				return this.selected_countries[0]+', '+this.selected_countries[1]+' + '+(this.selected_countries.length-2)
			} else {
				if(this.selected_countries.length == 0){
					return 'Все'
				} else {
					return this.selected_countries.join(', ')
				}
			}
		},
		countryChecked(country){
			return this.selected_countries.indexOf(country) > -1
		},
		getData(){
			this.isLoading = true
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				}
			});
			$.ajax({
				url: '/app/sinodik/public',
				type: 'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
				data: { range: this.selected_range.value, covid: this.selected_covid.value, countries: this.selected_countries },
				success: (data) => {
					this.data = data.by_country
					this.total = data.total

					if(data.by_country.length == 0){
						this.zeroResult = true
					} else {
						this.zeroResult = false
					}

					this.isLoading = false
				}
			})
		},
		titleLang(count){
			if(count > 10 && count < 15){
				return ' имен';
			} else {
				switch(count-Math.floor(count/10)*10){
					case 1:
						return ' имя';
						break;
					case 2:
					case 3:
					case 4:
						return ' имени';
						break;
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 0:
						return ' имен';
						break;
				}
			}
		}
	}
}).component('sinodik-list', {
	template: '#sinodik-list',
	data(){
		return {
			ranges: [
				{ title: 'Последние 7 дней', value: 'seven_days' },
				{ title: 'Последние 40 дней', value: 'fourty_days' },
				{ title: 'Последние 3 месяца', value: 'three_months' },
				{ title: 'Последние 6 месяцев', value: 'six_months' },
				{ title: 'Последний год', value: 'one_year' },
				{ title: '2021 год', value: '2021_year' },
				{ title: '2020 год', value: '2020_year' },
				{ title: 'Все', value: 'all' }
			],
			selected_range: { title: 'Последние 40 дней', value: 'fourty_days' },
			covid_options: [
				{ title: 'Ковид', value: 'covid' },
				{ title: 'Не ковид', value: 'noncovid' },
				{ title: 'Все', value: 'all' },
			],
			selected_covid: { title: 'Все', value: 'all' },
			countries: window.countries.filter(value => value.length > 0),
			selected_countries: ['Россия'],
			data_struct: {},
			data_unstruct: {},
			total: 0,
			isLoading: false,
			zeroResult: false,
			mod_rp: false,
			mod_lastname: false,
			mod_country: false
		}
	},
	computed:{
		countries_dropdown(){
			let mod = this.countries.filter(item => {
				return item != 'Россия' && item != 'Украина' && item != 'Беларусь'
			})

			mod.unshift('Беларусь')
			mod.unshift('Украина')
			mod.unshift('Россия')

			return mod
		}
	},
	mounted(){
		this.getData()
	},
	methods:{
		setRange(range){
			this.selected_range = range
			this.getData()
		},
		setCovid(option){
			this.selected_covid = option
			this.getData()
		},
		setCountry(country, single = false){
			if(this.selected_countries.indexOf(country) > -1){
				this.selected_countries.splice(this.selected_countries.indexOf(country), 1)
			} else {
				if(single){
					this.selected_countries = [country]
				} else {
					this.selected_countries.push(country)
				}
			}
			this.selected_countries.sort()

			this.getData()
		},
		countriesLabel(){
			if(this.selected_countries.length > 2){
				return this.selected_countries[0]+', '+this.selected_countries[1]+' + '+(this.selected_countries.length-2)
			} else {
				if(this.selected_countries.length == 0){
					return 'Все'
				} else {
					return this.selected_countries.join(', ')
				}
			}
		},
		countryChecked(country){
			return this.selected_countries.indexOf(country) > -1
		},
		getData(){
			this.isLoading = true
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				}
			});
			$.ajax({
				url: '/app/sinodik/list/public',
				type: 'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
				data: { range: this.selected_range.value, covid: this.selected_covid.value, countries: this.selected_countries },
				success: (data) => {
					this.data_struct = data.data_struct
					this.data_unstruct = data.data_unstruct
					this.total = data.total

					if(data.data_struct.length == 0 && data.data_unstruct.length == 0){
						this.zeroResult = true
					} else {
						this.zeroResult = false
					}

					this.isLoading = false
				}
			})
		},
	}
}).mount('#sinodik-app')