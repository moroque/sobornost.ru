(self["webpackChunk"] = self["webpackChunk"] || []).push([["downloads_log"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/DownloadsLog.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/DownloadsLog.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'DownloadsLog',
  title: 'Лог загрузок',
  data: function data() {
    return {
      downloads: [],
      isPaginated: true,
      isPaginationSimple: false,
      isPaginationRounded: false,
      paginationPosition: 'bottom',
      defaultSortDirection: 'desc',
      sortIcon: 'arrow-up',
      sortIconSize: 'is-small',
      page: 1,
      perPage: 50,
      total: 0,
      loading: false,
      sortField: 'created_at',
      sortOrder: 'desc'
    };
  },
  mounted: function mounted() {
    this.loadAsyncData();
  },
  methods: {
    loadAsyncData: function loadAsyncData() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.post('/app/downloads', {
                  sortField: _this.sortField,
                  sortOrder: _this.sortOrder,
                  page: _this.page,
                  per_page: _this.perPage
                }).then(function (r) {
                  _this.downloads = r.data.downloads;
                  _this.total = r.data.downloads.total;
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    onPageChange: function onPageChange(page) {
      this.page = page;
      this.loadAsyncData();
    },
    onSort: function onSort(field, order) {
      this.sortField = field;
      this.sortOrder = order;
      this.loadAsyncData();
    }
  }
});

/***/ }),

/***/ "./resources/js/components/DownloadsLog.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/DownloadsLog.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _DownloadsLog_vue_vue_type_template_id_62b755c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DownloadsLog.vue?vue&type=template&id=62b755c4& */ "./resources/js/components/DownloadsLog.vue?vue&type=template&id=62b755c4&");
/* harmony import */ var _DownloadsLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DownloadsLog.vue?vue&type=script&lang=js& */ "./resources/js/components/DownloadsLog.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _DownloadsLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _DownloadsLog_vue_vue_type_template_id_62b755c4___WEBPACK_IMPORTED_MODULE_0__.render,
  _DownloadsLog_vue_vue_type_template_id_62b755c4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/DownloadsLog.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/DownloadsLog.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/DownloadsLog.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DownloadsLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DownloadsLog.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/DownloadsLog.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DownloadsLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/DownloadsLog.vue?vue&type=template&id=62b755c4&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/DownloadsLog.vue?vue&type=template&id=62b755c4& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DownloadsLog_vue_vue_type_template_id_62b755c4___WEBPACK_IMPORTED_MODULE_0__.render,
/* harmony export */   "staticRenderFns": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DownloadsLog_vue_vue_type_template_id_62b755c4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DownloadsLog_vue_vue_type_template_id_62b755c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DownloadsLog.vue?vue&type=template&id=62b755c4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/DownloadsLog.vue?vue&type=template&id=62b755c4&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/DownloadsLog.vue?vue&type=template&id=62b755c4&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/DownloadsLog.vue?vue&type=template&id=62b755c4& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* binding */ render,
/* harmony export */   "staticRenderFns": () => /* binding */ staticRenderFns
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "section" }, [
    _vm.$root.user_can("view", "downloads")
      ? _c(
          "div",
          { staticClass: "container" },
          [
            _c("h1", { staticClass: "title" }, [
              _vm._v("Лог загрузок pdf-файла")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-3" }, [
              _vm._v(
                "\n\t\t\t\tСписок дедуплицирован на 01.02.2021, время московское\n\t\t\t"
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-3" }, [
              _vm._v("\n\t\t\t\tВсего: " + _vm._s(_vm.total) + "\n\t\t\t")
            ]),
            _vm._v(" "),
            _c(
              "b-table",
              {
                attrs: {
                  data: _vm.downloads.data,
                  paginated: "",
                  "per-page": _vm.perPage,
                  "current-page": _vm.page,
                  "pagination-simple": _vm.isPaginationSimple,
                  "pagination-position": _vm.paginationPosition,
                  "default-sort-direction": _vm.defaultSortDirection,
                  "pagination-rounded": _vm.isPaginationRounded,
                  "sort-icon": _vm.sortIcon,
                  "sort-icon-size": _vm.sortIconSize,
                  "default-sort": "created_at",
                  "aria-next-label": "Следующая",
                  "aria-previous-label": "Предыдущая",
                  "aria-page-label": "Страница",
                  "aria-current-label": "Текущая",
                  "backend-pagination": "",
                  total: _vm.total,
                  "backend-sorting": "",
                  "default-sort": [_vm.sortField, _vm.sortOrder]
                },
                on: {
                  "update:currentPage": function($event) {
                    _vm.page = $event
                  },
                  "update:current-page": function($event) {
                    _vm.page = $event
                  },
                  "page-change": _vm.onPageChange,
                  sort: _vm.onSort
                }
              },
              [
                _c("b-table-column", {
                  attrs: { field: "email", label: "Email / Имя", sortable: "" },
                  scopedSlots: _vm._u(
                    [
                      {
                        key: "default",
                        fn: function(props) {
                          return [
                            props.row.email == "direct_download"
                              ? [
                                  _vm._v(
                                    "\n\t\t\t\t\t\tПросто загрузка\n\t\t\t\t\t"
                                  )
                                ]
                              : [
                                  _c(
                                    "a",
                                    {
                                      attrs: {
                                        href: "mailto:" + props.row.email
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n\t\t\t\t\t\t\t" +
                                          _vm._s(props.row.email) +
                                          "\n\t\t\t\t\t\t"
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("small", [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t" +
                                        _vm._s(props.row.name) +
                                        " " +
                                        _vm._s(props.row.lastname) +
                                        "\n\t\t\t\t\t\t"
                                    )
                                  ])
                                ]
                          ]
                        }
                      }
                    ],
                    null,
                    false,
                    1145847943
                  )
                }),
                _vm._v(" "),
                _c("b-table-column", {
                  attrs: {
                    field: "created_at",
                    label: "Дата и время",
                    sortable: ""
                  },
                  scopedSlots: _vm._u(
                    [
                      {
                        key: "default",
                        fn: function(props) {
                          return [
                            _vm._v(
                              "\n\t\t\t\t\t" +
                                _vm._s(
                                  _vm
                                    .$moment(props.row.created_at)
                                    .format("LLL")
                                ) +
                                "\n\t\t\t\t"
                            )
                          ]
                        }
                      }
                    ],
                    null,
                    false,
                    570904970
                  )
                }),
                _vm._v(" "),
                _c("b-table-column", {
                  attrs: {
                    field: "ip",
                    label: "IP",
                    sortable: "",
                    centered: ""
                  },
                  scopedSlots: _vm._u(
                    [
                      {
                        key: "default",
                        fn: function(props) {
                          return [
                            _vm._v(
                              "\n\t\t\t\t\t" +
                                _vm._s(props.row.geo.ip) +
                                "\n\t\t\t\t"
                            )
                          ]
                        }
                      }
                    ],
                    null,
                    false,
                    3677755439
                  )
                }),
                _vm._v(" "),
                _c("b-table-column", {
                  attrs: { field: "country", label: "Страна", sortable: "" },
                  scopedSlots: _vm._u(
                    [
                      {
                        key: "default",
                        fn: function(props) {
                          return [
                            _c("span", [
                              _c("img", {
                                attrs: {
                                  width: "17",
                                  src: props.row.geo.location.country_flag
                                }
                              }),
                              _vm._v(
                                "\n\t\t\t\t\t\t" +
                                  _vm._s(props.row.country) +
                                  "\n\t\t\t\t\t"
                              )
                            ])
                          ]
                        }
                      }
                    ],
                    null,
                    false,
                    1869331139
                  )
                }),
                _vm._v(" "),
                _c("b-table-column", {
                  attrs: { field: "region", sortable: "", label: "Регион" },
                  scopedSlots: _vm._u(
                    [
                      {
                        key: "default",
                        fn: function(props) {
                          return [
                            _vm._v(
                              "\n\t\t\t\t\t" +
                                _vm._s(props.row.region) +
                                "\n\t\t\t\t"
                            )
                          ]
                        }
                      }
                    ],
                    null,
                    false,
                    4218789069
                  )
                }),
                _vm._v(" "),
                _c("b-table-column", {
                  attrs: { field: "city", sortable: "", label: "Нас. пункт" },
                  scopedSlots: _vm._u(
                    [
                      {
                        key: "default",
                        fn: function(props) {
                          return [
                            _c(
                              "a",
                              {
                                attrs: {
                                  href:
                                    "https://yandex.ru/maps/?ll=" +
                                    props.row.geo.longitude +
                                    "%2C" +
                                    props.row.geo.latitude +
                                    "&z=12",
                                  target: "_blank"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                        " +
                                    _vm._s(props.row.city) +
                                    "\n                    "
                                )
                              ]
                            )
                          ]
                        }
                      }
                    ],
                    null,
                    false,
                    1450316404
                  )
                }),
                _vm._v(" "),
                _c(
                  "template",
                  { slot: "bottom-left" },
                  [
                    _c(
                      "b-select",
                      {
                        attrs: { disabled: !_vm.isPaginated },
                        on: { input: _vm.loadAsyncData },
                        model: {
                          value: _vm.perPage,
                          callback: function($$v) {
                            _vm.perPage = $$v
                          },
                          expression: "perPage"
                        }
                      },
                      [
                        _c("option", { attrs: { value: "5" } }, [
                          _vm._v("5 на странице")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "10" } }, [
                          _vm._v("10 на странице")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "15" } }, [
                          _vm._v("15 на странице")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "20" } }, [
                          _vm._v("20 на странице")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "50" } }, [
                          _vm._v("50 на странице")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "100" } }, [
                          _vm._v("100 на странице")
                        ])
                      ]
                    )
                  ],
                  1
                )
              ],
              2
            )
          ],
          1
        )
      : _c("div", { staticClass: "container" }, [
          _c("h1", { staticClass: "title" }, [_vm._v("Доступ запрещен")]),
          _vm._v(" "),
          _c("p", [_vm._v("\n\t\t\t\tЭтот раздел недоступен\n\t\t\t")])
        ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);