(self["webpackChunk"] = self["webpackChunk"] || []).push([["about"],{

/***/ "./resources/js/components/About.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/About.vue ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _About_vue_vue_type_template_id_fb05e49c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./About.vue?vue&type=template&id=fb05e49c& */ "./resources/js/components/About.vue?vue&type=template&id=fb05e49c&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__.default)(
  script,
  _About_vue_vue_type_template_id_fb05e49c___WEBPACK_IMPORTED_MODULE_0__.render,
  _About_vue_vue_type_template_id_fb05e49c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/About.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/About.vue?vue&type=template&id=fb05e49c&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/About.vue?vue&type=template&id=fb05e49c& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_About_vue_vue_type_template_id_fb05e49c___WEBPACK_IMPORTED_MODULE_0__.render,
/* harmony export */   "staticRenderFns": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_About_vue_vue_type_template_id_fb05e49c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_About_vue_vue_type_template_id_fb05e49c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./About.vue?vue&type=template&id=fb05e49c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/About.vue?vue&type=template&id=fb05e49c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/About.vue?vue&type=template&id=fb05e49c&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/About.vue?vue&type=template&id=fb05e49c& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* binding */ render,
/* harmony export */   "staticRenderFns": () => /* binding */ staticRenderFns
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "section" }, [
      _c("div", { staticClass: "container" }, [
        _c("h1", { staticClass: "title" }, [_vm._v("Technical info")]),
        _vm._v(" "),
        _c("h3", { staticClass: "title is-4" }, [_vm._v("System")]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-3" }, [
          _c("b", [_vm._v("OS:")]),
          _vm._v(" Debian GNU/Linux 9.13 (stretch)"),
          _c("br"),
          _vm._v(
            "\n            is a LXC container on host ProxMox installation"
          ),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Web server:")]),
          _vm._v(" Nginx 1.18.0-2~stretch"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Database server:")]),
          _vm._v(" mariadb-server 10.3.27+maria~stretch"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Interpreter:")]),
          _vm._v(" PHP 7.4+79+0~20201210.30+debian9~1.gbpedaf15"),
          _c("br")
        ]),
        _vm._v(" "),
        _c("h3", { staticClass: "title is-4" }, [_vm._v("Backend")]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-3" }, [
          _c("b", [_vm._v("Framework:")]),
          _vm._v(" Laravel 8.12"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Image manipulation:")]),
          _vm._v(" intervention/image ^2.5"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Author maintanence:")]),
          _vm._v(" sqits/laravel-userstamps ^0.0.9"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Morphology:")]),
          _vm._v(" wapmorgan/morphos ^3.2"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Authentification:")]),
          _vm._v(" laravel built-in"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Excel export:")]),
          _vm._v(" laravel-excel ^3.1\n            "),
          _c("b", [_vm._v("RBAC:")]),
          _vm._v(" spatie/laravel-permission ^4.0\n        ")
        ]),
        _vm._v(" "),
        _c("h3", { staticClass: "title is-4" }, [_vm._v("Public frontend")]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-3" }, [
          _c("b", [_vm._v("Frontend framework:")]),
          _vm._v(" bootstrap 4.4.1 + jQuery 3.4.1"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Frontend style:")]),
          _vm._v(
            " modified DevBook v1.1 (Xiaoying Riley at 3rd Wave Media http://themes.3rdwavemedia.com/)"
          ),
          _c("br")
        ]),
        _vm._v(" "),
        _c("h3", { staticClass: "title is-4" }, [_vm._v("Admin frontend")]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-3" }, [
          _c("b", [_vm._v("Frontend framework:")]),
          _vm._v(" vue ^2.5.17 + vue-router ^3.4.9"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Frontend css framework:")]),
          _vm._v(" Bulma using buefy ^0.9.4"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Datetime manipulation:")]),
          _vm._v(" moment ^2.29.1"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Async requests:")]),
          _vm._v(" axios ^0.21"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Helpers:")]),
          _vm._v(" lodash ^4.17.19"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Charts:")]),
          _vm._v(" vue-chartjs ^3.5.1"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Input mask:")]),
          _vm._v(" vue-the-mask ^0.11.1"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Build system:")]),
          _vm._v(" laravel-mix ^6.0.10"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Javascript runtime:")]),
          _vm._v(" NodeJS 15.7.0"),
          _c("br"),
          _vm._v(" "),
          _c("b", [_vm._v("Nodejs package manager:")]),
          _vm._v(" npm 6.14.10"),
          _c("br")
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Public repo https://bitbucket.org/moroque/sobornost.ru/src/master/"
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Regular build flow: git stash && git pull origin master && npm run prod"
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "System has been built and is maintained by Alexander Morozov. Any questions - a@moro.me"
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ })

}]);