(self["webpackChunk"] = self["webpackChunk"] || []).push([["docs"],{

/***/ "./resources/js/components/Docs.vue":
/*!******************************************!*\
  !*** ./resources/js/components/Docs.vue ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _Docs_vue_vue_type_template_id_5255f8b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Docs.vue?vue&type=template&id=5255f8b4& */ "./resources/js/components/Docs.vue?vue&type=template&id=5255f8b4&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__.default)(
  script,
  _Docs_vue_vue_type_template_id_5255f8b4___WEBPACK_IMPORTED_MODULE_0__.render,
  _Docs_vue_vue_type_template_id_5255f8b4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Docs.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Docs.vue?vue&type=template&id=5255f8b4&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/Docs.vue?vue&type=template&id=5255f8b4& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Docs_vue_vue_type_template_id_5255f8b4___WEBPACK_IMPORTED_MODULE_0__.render,
/* harmony export */   "staticRenderFns": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Docs_vue_vue_type_template_id_5255f8b4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Docs_vue_vue_type_template_id_5255f8b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Docs.vue?vue&type=template&id=5255f8b4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Docs.vue?vue&type=template&id=5255f8b4&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Docs.vue?vue&type=template&id=5255f8b4&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Docs.vue?vue&type=template&id=5255f8b4& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* binding */ render,
/* harmony export */   "staticRenderFns": () => /* binding */ staticRenderFns
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "section" }, [
      _c("div", { staticClass: "container" }, [
        _c("h1", { staticClass: "title" }, [
          _vm._v("Документация и инструкции")
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "\n            Этот раздел содержит видео с инструкциями по работе с системой\n        "
          )
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "title is-4 mt-4" }, [
          _vm._v("Документация и инструкции")
        ]),
        _vm._v(" "),
        _c("iframe", {
          attrs: {
            width: "560",
            height: "315",
            src: "https://www.youtube.com/embed/-LMW9WnejII",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        }),
        _vm._v(" "),
        _c("h4", { staticClass: "title is-4 mt-4" }, [
          _vm._v("Раздел Лог загрузок")
        ]),
        _vm._v(" "),
        _c("iframe", {
          attrs: {
            width: "560",
            height: "315",
            src: "https://www.youtube.com/embed/0AekBUv4n-8",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        }),
        _vm._v(" "),
        _c("h4", { staticClass: "title is-4 mt-4" }, [_vm._v("Раздел Отзывы")]),
        _vm._v(" "),
        _c("iframe", {
          attrs: {
            width: "560",
            height: "315",
            src: "https://www.youtube.com/embed/Vo3DBLWRJxg",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        }),
        _vm._v(" "),
        _c("h4", { staticClass: "title is-4 mt-4" }, [
          _vm._v("Раздел Синодик")
        ]),
        _vm._v(" "),
        _c("iframe", {
          attrs: {
            width: "560",
            height: "315",
            src: "https://www.youtube.com/embed/vLGtoMvhark",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        }),
        _vm._v(" "),
        _c("h4", { staticClass: "title is-4 mt-4" }, [
          _vm._v("Технические разделы")
        ]),
        _vm._v(" "),
        _c("iframe", {
          attrs: {
            width: "560",
            height: "315",
            src: "https://www.youtube.com/embed/6_JRZcz9Pzo",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        }),
        _vm._v(" "),
        _c("h4", { staticClass: "title is-4 mt-4" }, [
          _vm._v("Раздел Пользователи")
        ]),
        _vm._v(" "),
        _c("iframe", {
          attrs: {
            width: "560",
            height: "315",
            src: "https://www.youtube.com/embed/6CXCfMZZMdY",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        })
      ])
    ])
  }
]
render._withStripped = true



/***/ })

}]);